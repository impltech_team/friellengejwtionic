import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the SortPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'sort',
})
export class SortPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(array: Array<string>, args?: any): Array<string> {
    if (array !== undefined) {
      return array.sort(function (a, b) {
        // console.log("SORTING PROFILE ",a['profile'].fullName);
        // console.log("SORTING PROFILE 2",a[''+args.property].fullName);
        //
        // let aa =  a['profile'];
        // console.log("SORTING PROFILE 3",a['profile']['fullName']);
        // console.log("SORTING PROFILE 4",aa['fullName']);
        if(args.subproperty != null && args.subproperty != '') {

          if (a[args.property][args.subproperty] < b[args.property][args.subproperty]) {
            return -1 * args.order;
          }
          else if (a[args.property][args.subproperty] > b[args.property][args.subproperty]) {
            return 1 * args.order;
          }
          else {
            return 0;
          }
        }
        else {
          if (a[args.property] < b[args.property]) {
            return -1 * args.order;
          }
          else if (a[args.property] > b[args.property]) {
            return 1 * args.order;
          }
          else {
            return 0;
          }
        }


      });
    }
  }

}
