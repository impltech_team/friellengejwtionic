export class Template {
  constructor(
    public id?: number,
    public type?: string,
    public description?: string,
    public contentReference?: string,
  ) {
  }
}
