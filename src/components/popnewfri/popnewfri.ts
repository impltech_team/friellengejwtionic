import { Component } from '@angular/core';
import {ViewController} from "ionic-angular";

/**
 * Generated class for the PopnewfriComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'popnewfri',
  templateUrl: 'popnewfri.html'
})
export class PopnewfriComponent {

  items: any;
  text: string;

  constructor(public viewCtrl: ViewController) {
    this.items = [
      {item: 'Галерея'},
      {item: 'Камера'}
    ]
  }


  itemClick(item){
    this.viewCtrl.dismiss(item);
  }

}
