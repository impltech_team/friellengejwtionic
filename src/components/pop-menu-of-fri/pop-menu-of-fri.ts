import { Component } from '@angular/core';
import {ViewController} from "ionic-angular";

/**
 * Generated class for the PopMenuOfFriComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'pop-menu-of-fri',
  templateUrl: 'pop-menu-of-fri.html'
})
export class PopMenuOfFriComponent {

  items: any;
  text: string;

  constructor(public viewCtrl: ViewController) {
    this.items = [
      {item: 'Репорт'},
      {item: 'Донат'}
    ]
  }

  itemClick(item){
    this.viewCtrl.dismiss(item);
  }

}
