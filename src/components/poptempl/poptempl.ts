import { Component } from '@angular/core';
import {ViewController} from "ionic-angular";

/**
 * Generated class for the PoptemplComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'poptempl',
  templateUrl: 'poptempl.html'
})
export class PoptemplComponent {

  items: any;
  text: string;

  constructor(public viewCtrl: ViewController) {
    this.items = [
      {item: 'друзья'},
      {item: 'работа'},
      {item: 'учеба'},
      {item: 'любовь'},
      {item: 'семья'},
      {item: 'прочее'},
      {item: 'свой'}
    ]
  }

  itemClick(item){
    this.viewCtrl.dismiss(item);
  }

}
