import { NgModule } from '@angular/core';
import { PoptemplComponent } from './poptempl/poptempl';
import { PopnewfriComponent } from './popnewfri/popnewfri';
import { PopMenuOfFriComponent } from './pop-menu-of-fri/pop-menu-of-fri';
import { PopMenuOfFriAuthorComponent } from './pop-menu-of-fri-author/pop-menu-of-fri-author';
import { PopMenuOfComAuthorComponent } from './pop-menu-of-com-author/pop-menu-of-com-author';
import { PopMenuOfComComponent } from './pop-menu-of-com/pop-menu-of-com';

@NgModule({
	declarations: [
    PoptemplComponent,
    PopnewfriComponent,
    PopMenuOfFriComponent,
    PopMenuOfFriAuthorComponent,
    PopMenuOfComAuthorComponent,
    PopMenuOfComComponent,

  ],
	imports: [],
	exports: [
    PoptemplComponent,
    PopnewfriComponent,
    PopMenuOfFriComponent,
    PopMenuOfFriAuthorComponent,
    PopMenuOfComAuthorComponent,
    PopMenuOfComComponent,

  ]
})
export class ComponentsModule {}
