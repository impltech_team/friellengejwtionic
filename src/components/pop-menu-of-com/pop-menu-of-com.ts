import { Component } from '@angular/core';
import {ViewController} from "ionic-angular";

/**
 * Generated class for the PopMenuOfComComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'pop-menu-of-com',
  templateUrl: 'pop-menu-of-com.html'
})
export class PopMenuOfComComponent {

  items: any;
  text: string;

  constructor(public viewCtrl: ViewController) {
    this.items = [
      {item: 'Репорт'},
    ]
  }

  itemClick(item){
    this.viewCtrl.dismiss(item);
  }

}
