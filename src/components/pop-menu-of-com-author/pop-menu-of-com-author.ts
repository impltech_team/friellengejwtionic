import { Component } from '@angular/core';
import {ViewController} from "ionic-angular";

/**
 * Generated class for the PopMenuOfComAuthorComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'pop-menu-of-com-author',
  templateUrl: 'pop-menu-of-com-author.html'
})
export class PopMenuOfComAuthorComponent {
  items: any;
  text: string;

  constructor(public viewCtrl: ViewController) {
    this.items = [
      {item: 'Удалить'}
    ]
  }

  itemClick(item){
    this.viewCtrl.dismiss(item);
  }
}
