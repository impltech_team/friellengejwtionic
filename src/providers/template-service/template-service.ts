import { Injectable } from '@angular/core';
import {Http, Response} from "@angular/http";
import {Observable} from "rxjs/Observable";
import {ResponseWrapper} from "../../models/response-wrapper.model";
import {createRequestOption} from "../../models/request-util";
import {Template} from "../../models/template.model";
import {Api} from "../api/api";

@Injectable()
export class TemplateServiceProvider {
  // private resourceUrl = 'http://localhost:8080/api/templates';
  private resourceGet = 'http://' + Api.SERVER_URL + '/api/templatesByType/';

  constructor(public http: Http) {
  }

  query(typeOfTemplate:any, req?: any): Observable<ResponseWrapper> {
    const options = createRequestOption(req);
    return this.http.get(this.resourceGet + typeOfTemplate, options)
      .map((res: Response) => this.convertResponse(res));
  }

  private convertResponse(res: Response): ResponseWrapper {
    const jsonResponse = res.json();
    const result = [];
    for (let i = 0; i < jsonResponse.length; i++) {
      result.push(this.convertItemFromServer(jsonResponse[i]));
    }
    return new ResponseWrapper(res.headers, result, res.status);
  }

  /**
   * Convert a returned JSON object to Template.
   */
  private convertItemFromServer(json: any): Template {
    const entity: Template = Object.assign(new Template(), json);
    return entity;
  }

  /**
   * Convert a Orders to a JSON which can be sent to the server.
   */
}
