import { Injectable } from '@angular/core';
import {ResponseWrapper} from "../../models/response-wrapper.model";
import {Observable} from "rxjs/Observable";
import {createRequestOption} from "../../models/request-util";
import {Http, Response} from '@angular/http';
import {Headers, RequestOptions} from '@angular/http';
import {CommentModel} from "../../pages/comment/comment.model";
import {Api} from "../api/api";

@Injectable()
export class CommentProvider {
  private resourceUrlAdd = 'http://' + Api.SERVER_URL + '/api/comments';
  private resourceUrlGet = 'http://' + Api.SERVER_URL + '/api/commentsByFriellenge/';
  private resourceUrlDeleteById = 'http://' + Api.SERVER_URL + '/api/comments';


  constructor(public http: Http) {
  }

  query(friellengeId: number, req?: any): Observable<ResponseWrapper> {
    const options = createRequestOption(req);
    console.log(friellengeId);
    return this.http.get((this.resourceUrlGet + friellengeId), options)
      .map((res: Response) => this.convertResponse(res));
  }

  private convertResponse(res: Response): ResponseWrapper {
    const jsonResponse = res.json();
    const result = [];
    for (let i = 0; i < jsonResponse.length; i++) {
      result.push(this.convertItemFromServer(jsonResponse[i]));
    }
    return new ResponseWrapper(res.headers, result, res.status);
  }

  addNewComment(comment: CommentModel, req?: any) : Observable<Response> {
    let body = JSON.stringify(comment);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.resourceUrlAdd, body, options);
  }

  deleteCommentById(commentId: number): Observable<Response> {
    return this.http.delete(this.resourceUrlDeleteById +"/" + commentId);

  }

  /**
   * Convert a returned JSON object to CommentModel.
   */
  private convertItemFromServer(json: any): CommentModel {
    const entity: CommentModel = Object.assign(new CommentModel(), json);
    return entity;
  }

  /**
   * Convert a Orders to a JSON which can be sent to the server.
   */
}
