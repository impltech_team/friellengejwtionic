// import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from "rxjs/Rx";
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {ApprovalModel} from "../../pages/approval/approval.model";
import {Api} from "../api/api";
import {ResponseWrapper} from "../../models/response-wrapper.model";
import {createRequestOption} from "../../models/request-util";

/*
  Generated class for the ApprovalServiceProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ApprovalServiceProvider {

  private resourceUrlForJoin = 'http://' + Api.SERVER_URL + '/api/approvals';
  private resourceUrlForDelete = 'http://' + Api.SERVER_URL + '/api/approvals';
  private resourceUrlForUpdate = 'http://' + Api.SERVER_URL + '/api/approvalsUpd';
  private resourceUrlForGetByProfile = 'http://' + Api.SERVER_URL + '/api/approvalsByProfile/';
  private resourceUrlForGetByFriellenge = 'http://' + Api.SERVER_URL + '/api/approvalsByFriellenge';
  private resourceUrlForGetByFriellengeAndProfile = 'http://' + Api.SERVER_URL + '/api/approvalsByFriellengeAndProfile';
  private resourceUrlForApprovalLikes = 'http://' + Api.SERVER_URL + '/api/approvalLikes';
  private resourceUrlForApprovalLikesAll = 'http://' + Api.SERVER_URL + '/api/approvalLikesAll';

  // private resourceUrl = 'http://localhost:8080/api/approvals?friellengeId=1&profileId=1'

  constructor(public http: Http) {
  }

  getByProfile(profileId: number, req?: any): Observable<ResponseWrapper> {
    const options = createRequestOption(req);
    console.log("approval query profile id ",profileId);
    return this.http.get((this.resourceUrlForGetByProfile + profileId), options)
      .map((res: Response) => this.convertResponse(res));
  }

  getByFriellenge(friellengeId: number, profileId: number, req?: any): Observable<ResponseWrapper> {
    const options = createRequestOption(req);
    console.log("approval query friellenge id ",friellengeId);
    return this.http.get((this.resourceUrlForGetByFriellenge  +"?friellengeId=" + friellengeId + "&profileId=" + profileId), options)
      .map((res: Response) => this.convertResponse(res));
  }

  getByFriellengeAndProfile(friellengeId: number, profileId: number, req?: any): Observable<ApprovalModel>{
    // const options = createRequestOption(req);
    console.log("approval query friellenge id and profile id",friellengeId," ", profileId);
    return this.http.get((this.resourceUrlForGetByFriellengeAndProfile +"?friellengeId=" + friellengeId + "&profileId=" + profileId))
      .map((res: Response) => {
        const jsonResponse = res.json();
        return this.convertItemFromServer(jsonResponse);
      });
  }

  addNewApproval(approval: ApprovalModel, req?: any) : Observable<Response> {
    let body = JSON.stringify(approval);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.resourceUrlForJoin, body, options);
  }

  update(approval: ApprovalModel): Observable<Response> {
    let body = JSON.stringify(approval);
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.put(this.resourceUrlForUpdate, body, options);
  }

  deleteApprovalsByFriellengeIdAndProfileId(friellengeId: number, profileId: number): Observable<Response> {
    console.log(friellengeId + " " + profileId);
    return this.http.delete(this.resourceUrlForJoin +"?friellengeId=" + friellengeId + "&profileId=" + profileId);
  }

  deleteApprovalById(approvalId: number): Observable<Response> {
    return this.http.delete(this.resourceUrlForDelete +"/" + approvalId);

  }

  private convertResponse(res: Response): ResponseWrapper {
    console.log("Approvals convert ", res);

    const jsonResponse = res.json();
    const result = [];
    for (let i = 0; i < jsonResponse.length; i++) {
      result.push(this.convertItemFromServer(jsonResponse[i]));
    }
    return new ResponseWrapper(res.headers, result, res.status);
  }

  private convertItemFromServer(json: any): ApprovalModel {
    console.log("convertItemFromServer ", json);

    const entity: ApprovalModel = Object.assign(new ApprovalModel(), json);
    return entity;
  }

  addLikeByApprovalIdAndProfileId(approvalId: number, profileId: number) : Observable<Response> {
    console.log(approvalId + " " + profileId);
    return this.http.post((this.resourceUrlForApprovalLikes +"?approvalId=" + approvalId + "&profileId=" + profileId), null);
  }

  deleteLikeByApprovalIdAndProfileId(approvalId: number, profileId: number): Observable<Response> {
    console.log(approvalId + " " + profileId);
    return this.http.delete(this.resourceUrlForApprovalLikes +"?approvalId=" + approvalId + "&profileId=" + profileId);
  }

  deleteAllLikesByApproval(approvalId: number): Observable<Response>{
    console.log('delete all likes in approval ',approvalId);
    return this.http.delete(this.resourceUrlForApprovalLikesAll +"?approvalId=" + approvalId);
  }

}
