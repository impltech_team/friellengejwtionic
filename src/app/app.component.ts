import { Component, ViewChild } from '@angular/core';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { TranslateService } from '@ngx-translate/core';
import { Config, Nav, Platform } from 'ionic-angular';
import { Settings } from '../providers/providers';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'WelcomePage';
  // activePage: any;
  //
  // pages: Array<{title: string, component: any}>;

  constructor(private translate: TranslateService, platform: Platform, settings: Settings, private config: Config,
              private statusBar: StatusBar, private splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
    this.initTranslate();



    // this.pages = [
    //   // { title: 'Welcome', component: 'WelcomePage' },
    //   // { title: 'Tabs', component: 'TabsPage' },
    //   { title: 'Login', component: 'LoginPage' },
    //   // { title: 'Signup', component: 'SignupPage' },
    //   // { title: 'Menu', component: 'MenuPage' },
    //   { title: 'Active', component: 'ActivePage' },
    //   { title: 'Best', component: 'BestPage' },
    //   { title: 'My', component: 'MyPage' },
    //   { title: 'Favourite', component: 'FavouritePage' },
    //   { title: 'Profile', component: 'ProfilePage' },
    //   { title: 'Settings', component: 'SettingsPage' },
    //   { title: 'Friellenge', component: 'FriellengePage'},
    //   { title: 'CommentModel', component: 'CommentPage'},
    //   { title: 'Template', component: 'TemplatePage'},
    //   // { title: 'Entities', component: 'EntityPage' }
    // ];
    //
    // this.activePage = this.pages[0];

  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');

    if (this.translate.getBrowserLang() !== undefined) {
      this.translate.use(this.translate.getBrowserLang());
    } else {
      this.translate.use('en'); // Set your language here
    }

    this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
      this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
    });
  }

  // openPage(page) {
  //   // Reset the content nav to have just this page
  //   // we wouldn't want the back button to show in this scenario
  //   this.nav.setRoot(page.component);
  //   this.activePage = page;
  // }
  //
  // checkActive(page){
  //   return page == this.activePage;
  // }

}
