import { HTTP_INTERCEPTORS, HttpClient, HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Camera } from '@ionic-native/camera';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicStorageModule, Storage } from '@ionic/storage';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Api, Settings, User } from '../providers/providers';
import { MyApp } from './app.component';
import { LoginService } from '../providers/login/login.service';
import { Principal } from '../providers/auth/principal.service';
import { AccountService } from '../providers/auth/account.service';
import { AuthServerProvider } from '../providers/auth/auth-jwt.service';
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { AuthInterceptor } from '../providers/auth/auth-interceptor';
import { EntityPageModule } from '../pages/entities/entity.module';
import {HttpModule} from "@angular/http";
import { CommentProvider } from '../providers/comment/comment';
import { TemplateServiceProvider } from '../providers/template-service/template-service';
import { Facebook } from '@ionic-native/facebook';
import {ProfilePage} from "../pages/profile/profile";
import {ProfileService} from "../pages/profile/profile.service";
import { ApprovalServiceProvider } from '../providers/approval-service/approval-service';
import {FriellengePageModule} from "../pages/friellenge/friellenge.module";
import {SimpleTimerPage} from "../pages/simple-timer/simple-timer";
// import { SocialSharing} from "@ionic-native/social-sharing";
import {PopnewfriComponent} from "../components/popnewfri/popnewfri";
import {PoptemplComponent} from "../components/poptempl/poptempl";
import {PopMenuOfFriComponent} from "../components/pop-menu-of-fri/pop-menu-of-fri";
import {PopMenuOfFriAuthorComponent} from "../components/pop-menu-of-fri-author/pop-menu-of-fri-author";
import {OpenProfileService} from "../pages/open-profile/open-profile.service";
import {ReportPageModule} from "../pages/report/report.module";
import {ReportService} from "../pages/report/report.service";
import {Base64ToGallery} from "@ionic-native/base64-to-gallery";
import {FriellengeService} from "../pages/friellenge/friellenge.service";
import {FriellengePage} from "../pages/friellenge/friellenge";
import {PopMenuOfComAuthorComponent} from "../components/pop-menu-of-com-author/pop-menu-of-com-author";
import {PopMenuOfComComponent} from "../components/pop-menu-of-com/pop-menu-of-com";
import {CommentPage} from "../pages/comment/comment";
// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

export function provideSettings(storage: Storage) {
  /**
   * The Settings provider takes a set of default settings for your app.
   *
   * You can add new settings options at any time. Once the settings are saved,
   * these values will not overwrite the saved values (this can be done manually if desired).
   */
  return new Settings(storage, {
    option1: true,
    option2: 'Ionitron J. Framework',
    option3: '3',
    option4: 'Hello'
  });
}

@NgModule({
  declarations: [
    MyApp,
    SimpleTimerPage,
    PopnewfriComponent,
    PoptemplComponent,
    PopMenuOfFriComponent,
    PopMenuOfFriAuthorComponent,
    PopMenuOfComAuthorComponent,
    PopMenuOfComComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    }),
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    EntityPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    SimpleTimerPage,
    PopnewfriComponent,
    PoptemplComponent,
    PopMenuOfFriComponent,
    PopMenuOfFriAuthorComponent,
    PopMenuOfComAuthorComponent,
    PopMenuOfComComponent
  ],
  providers: [
    Api,
    User,
    LoginService,
    ProfilePage,
    ProfileService,
    SimpleTimerPage,
    Principal,
    AccountService,
    AuthServerProvider,
    LocalStorageService,
    SessionStorageService,
    Camera,
    SplashScreen,
    Facebook,
    // SocialSharing,
    StatusBar,
    { provide: Settings, useFactory: provideSettings, deps: [Storage] },
    // Keep this to enable Ionic's runtime error handling during development
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    CommentProvider,
    TemplateServiceProvider,
    ApprovalServiceProvider,
    FriellengePageModule,
    OpenProfileService,
    ReportService,
    Base64ToGallery,
    FriellengeService
  ]
})
export class AppModule { }
