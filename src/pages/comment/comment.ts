import {Component, OnDestroy, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams, PopoverController, ToastController} from 'ionic-angular';
import {CommentProvider} from "../../providers/comment/comment";
import {ResponseWrapper} from "../../models/response-wrapper.model";
import {CommentModel} from "./comment.model";
import {Friellenge} from "../friellenge/friellenge.model";
import {Profile} from "../profile/profile.model";
import {FriellengeService} from "../friellenge/friellenge.service";
import {PopMenuOfComComponent} from "../../components/pop-menu-of-com/pop-menu-of-com";
import {PopMenuOfComAuthorComponent} from "../../components/pop-menu-of-com-author/pop-menu-of-com-author";
import {ApprovalModel} from "../approval/approval.model";
import {ReportService} from "../report/report.service";




@IonicPage()
@Component({
  selector: 'page-comment',
  templateUrl: 'comment.html',
})
export class CommentPage implements OnInit, OnDestroy {
  friellenge: Friellenge;
  comment: CommentModel[];
  newComment: CommentModel = new CommentModel(null,'',);
  profile: Profile;
  profileId = JSON.parse(localStorage.getItem("profile"));

  ngOnInit() {
    this.loadAll();
  }

  ngOnDestroy() {}

  constructor(public popoverCtrl: PopoverController,
              public navCtrl: NavController,
              public navParams: NavParams,
              private friellengeService: FriellengeService,
              private commentProvider: CommentProvider,
              public reportService: ReportService,
              public toast: ToastController) {
    this.friellenge = navParams.get('friellenge');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommentPage');
  }

  loadAll() {
    this.commentProvider.query(this.friellenge.id).subscribe(
      (res: ResponseWrapper) => {
      this.comment = res.json;
      this.loadPicture();
    },);
  }

  save(){
    if(this.newComment.comment !== '') {
      this.newComment.commentDate = new Date;
      this.newComment.friellenge = new Friellenge(this.friellenge.id);
      this.profile = this.profileId;
      this.newComment.profile = this.profile;
      console.log(this.newComment);
      this.commentProvider.addNewComment(this.newComment).subscribe();
      this.addComment();
    }
    this.navCtrl.pop();

  }
  getDate(date: Date) {
    let da = new Date(date) ;
    // console.log(da.toISOString());
    // console.log(da.toUTCString());
    // console.log(da.toUTCString());
    // console.log(da.toLocaleString());
    return da.toLocaleString().substr(0, 17);

  }

  private getPicture(comments: CommentModel) {
    return this.friellengeService.loadPicture(comments.profile.linkPhoto).subscribe( (res) => {
      comments.profile.linkPhoto = 'data:image/jpeg;base64,' + res.text();
    });
  }

  private loadPicture() {
    for (let entry of this.comment) {
      this.getPicture(entry);
    }
  }

  addComment(){
    this.friellenge.countOfCommentByFriellenge++;
  }

  presentPopoverComment(event, comment: CommentModel) {
    let popover;
    if (comment.profile.id === this.profileId.id) {
      popover = this.popoverCtrl.create(PopMenuOfComAuthorComponent);
      console.log(popover);
      popover.present({
        ev: event
      });
    }
    else {
      popover = this.popoverCtrl.create(PopMenuOfComComponent);
      console.log("Com",popover);
      popover.present({
        ev: event
      });
    }
    popover.onDidDismiss(popoverData => {
      console.log(popoverData);
      if (popoverData != null && popoverData.item == 'Репорт')
        this.doRepost(this.friellenge, null, comment);
        // this.doRepost(comment, null);
       else if (popoverData != null && popoverData.item == 'Удалить')
        this.deleteComment(comment.id);
    });
  }

  private doRepost(item: Friellenge,approval: ApprovalModel, comment: CommentModel) { // todo it is need finish this method
    this.reportService.showCheckbox(item, approval, comment);
  }

  public deleteComment(commentId: number) {
    this.commentProvider.deleteCommentById(commentId).subscribe(
      (value) => {
        if (value.statusText == "OK") {
          this.showToast("Комментарий удален");
        }
      },
      (res: ResponseWrapper) => {
        console.log("Status ERROR", res);
        this.showToast("Ошибка сервера");
      },
      () => {
        console.log("Status OK and LOAD");
        this.loadAll();
      }
    );

  }

  showToast(name: string) {
    const toast = this.toast.create({
      message: `${name}`,
      duration: 2000
    });
    toast.present();
  }
}
