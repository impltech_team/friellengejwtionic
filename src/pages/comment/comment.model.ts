import {BaseEntity} from "../../models";
import {Friellenge} from "../friellenge/friellenge.model";
import {Profile} from "../profile/profile.model";

export class CommentModel implements BaseEntity {
  constructor(public id?: number,
              public comment?: string,
              public profile?: Profile,
              public commentDate?: Date,
              public friellenge?: Friellenge,) { }

}
