import { Component, OnInit, OnDestroy} from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-simple-timer',
  templateUrl: 'simple-timer.html'
})
export class SimpleTimerPage implements OnInit, OnDestroy {
  remainingSeconds: number;
  runTimer: boolean;
  hasStarted: boolean;
  hasFinished: boolean;
  displayTime: string;

  timeInSeconds: number;

  constructor(public navCtrl: NavController, public navParams: NavParams) {}

  ionViewDidLoad() {
    console.log('ionViewDidLoad SimpleTimerPage');
  }

  ngOnInit() {

    setTimeout(() => {
      this.startTimer();
    }, 1000)

    this.timeInSeconds=300;
    this.initTimer();
  }

  ngOnDestroy() {
  }

  initTimer() {
    if(!this.timeInSeconds) { this.timeInSeconds = 0; }

    this.runTimer = false;
    this.hasStarted = false;
    this.hasFinished = false;
    this.remainingSeconds = this.timeInSeconds;
    this.displayTime = this.getSecondsAsDigitalClock(this.remainingSeconds);
  }
  startTimer() {
    this.runTimer = true;
    this.hasStarted = true;
    this.timerTick();
  }
  pauseTimer() {
    this.runTimer = false;
  }
  resumeTimer() {
    this.startTimer();
  }
  timerTick() {
    setTimeout(() => {
      if (!this.runTimer) { return; }
      this.remainingSeconds--;
      this.displayTime = this.getSecondsAsDigitalClock(this.remainingSeconds);
      if (this.remainingSeconds > 0) {
        this.timerTick();
      }
      else {
        this.hasFinished = true;
      }
    }, 1000);
  }
  getSecondsAsDigitalClock(inputSeconds: number) {
    var sec_num = parseInt(inputSeconds.toString(), 10);
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);
    var hoursString = '';
    var minutesString = '';
    var secondsString = '';
    hoursString = (hours < 10) ? "0" + hours : hours.toString();
    minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
    secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
    return hoursString + ':' + minutesString + ':' + secondsString;
  }

  setTime(time) {
    this.timeInSeconds=time;

    this.pauseTimer();
    this.initTimer();

    // As startTimer() has the this.timerTick(), which we don't want to call here
    // otherwise two seconds will change per tick
    this.runTimer = true;
    this.hasStarted = true;
  }
}
