import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Rx";
import {Headers, Http, RequestOptions, Response} from "@angular/http";
import {Api} from "../../providers/api/api";
import {ReportModel} from "./report.model";
import {ApprovalModel} from "../approval/approval.model";
import {Friellenge} from "../friellenge/friellenge.model";
import {AlertController, ToastController} from "ionic-angular";
import {ResponseWrapper} from "../../models/response-wrapper.model";
import {CommentModel} from "../comment/comment.model";



@Injectable()
export class ReportService {
  newReport: ReportModel = new ReportModel(null);

  private resourceUrlReportAdd = 'http://' + Api.SERVER_URL + '/api/reports';
  private resourceUrlReportBoolean = 'http://' + Api.SERVER_URL + '/api/reportBoolean';

  constructor(private http: Http, private alertCtrl: AlertController, public toast: ToastController) {
  }

  createReport(report: ReportModel): Observable<Response> {
    let body = JSON.stringify(report);
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    return this.http.post(this.resourceUrlReportAdd, body,options);
  }

  getReportBooleanRequest(friellengeId: number, approvalId: number): Observable<string> {
    return this.http.get(this.resourceUrlReportBoolean + "?profileId=" + friellengeId + "&friendsId=" + approvalId)
      .map((res: Response) => res.text());
  }



  showCheckbox(item: Friellenge, approval: ApprovalModel, comment: CommentModel) {
    let alert = this.alertCtrl.create();
    alert.setTitle('Пожалаваться на картинку или название. Фриланж: '  + item.nameFriellenge
    );

    alert.addInput({
      type: 'radio',
      label: 'Содержит сцены сексуального характера',
      value: 'Содержит сцены сексуального характера',

    });

    alert.addInput({
      type: 'radio',
      label: 'Насильственное или угрожающее содержание',
      value: 'Насильственное или угрожающее содержание',
    });

    alert.addInput({
      type: 'radio',
      label: 'Оскорбительного содержания или Расжигание ненависти',
      value: 'Оскорбительного содержания или Расжигание ненависти',
    });

    alert.addInput({
      type: 'radio',
      label: 'Вредные или небезопастные действия',
      value: 'Вредные или небезопастные действия',
    });

    alert.addInput({
      type: 'radio',
      label: 'Спам или обманное содержание',
      value: 'Спам или обманное содержание',
    });

    alert.addButton('Отменить');
    alert.addButton({
      text: 'Пожаловаться',
      handler: data => {
        this.newReport.profile = item.profile;
        this.newReport.friellenge = item;
        if (approval == null) {
          this.newReport.approval = {};
        }else {
          this.newReport.approval = approval;
        }
        if(comment == null){
          this.newReport.comment = {};
        }else {
          this.newReport.comment = comment;
        }
        this.newReport.description = data;
        this.newReport.confirmed = false;

        this.createReport(this.newReport).subscribe((value)=> {
          if (value.statusText == "OK") {
            this.showToast("Жалоба отправлена")
          }
        },
          (res: ResponseWrapper)=>{
            this.showToast("Жалобу, данного содержания, Вы уже отправляли");
         })
      }
    });
    alert.present();
  }

  showToast(name: string){
    const toast = this.toast.create({
      message:`${name}`,
      duration: 3000
    });
    toast.present();
  }
}
