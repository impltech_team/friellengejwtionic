import {Profile} from "../profile/profile.model";
// import {BaseEntity} from "../../models";
import {Friellenge} from "../friellenge/friellenge.model";
import {ApprovalModel} from "../approval/approval.model";
import {CommentModel} from "../comment/comment.model";


export class ReportModel  {
  constructor(
    // public id?: number,
    public profile?: Profile,
    public friellenge?: Friellenge,
    public approval?: ApprovalModel,
    public comment?: CommentModel,
    public description?: string,
    public confirmed?: boolean,

  ) {
  }
}
