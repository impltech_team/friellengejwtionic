import {Injectable} from '@angular/core';
import {Observable} from "rxjs/Rx";
import {Http, Response} from "@angular/http";
import {Api} from "../../providers/api/api";



@Injectable()
export class OpenProfileService {

  private resourceUrlFriendsByProfileIdAndFriendsId = 'http://' + Api.SERVER_URL + '/api/friendsRequest';

  constructor(private http: Http) {
  }

  getFriendsRequest(profileId: number, friendsId: number): Observable<string> {
    return this.http.get(this.resourceUrlFriendsByProfileIdAndFriendsId + "?profileId=" + profileId + "&friendsId=" + friendsId)
      .map((res: Response) => res.text());

  }



}
