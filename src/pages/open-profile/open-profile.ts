import {Component, OnInit} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Profile} from "../profile/profile.model";
import {ProfileService} from "../profile/profile.service";
import {ResponseWrapper} from "../../models/response-wrapper.model";
import {OpenProfileService} from "./open-profile.service";
import {FriendsModel} from "../friends/friends.model";
import {FriellengeService} from "../friellenge/friellenge.service";


@IonicPage() @Component({
  selector: 'page-open-profile', templateUrl: 'open-profile.html',
})
export class OpenProfilePage implements OnInit {
  profiles: Profile[];
  profile: Profile;
  id: number;
  profileId = JSON.parse(localStorage.getItem("profile"));
  friendsId = this.navParams.get('profile');
  confirm = false;
  btn: boolean = true;
  btns: boolean = true;

  openProfile(profile: Profile) {
    this.navCtrl.push('OpenProfilePage', {profile: profile})
  }


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private profileService: ProfileService,
              private openProfileService: OpenProfileService,
              private alertCtrl: AlertController,
              public friellengeService: FriellengeService) {

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OpenProfilePage');
  }

  ngOnInit() {
    this.profile = this.navParams.get('profile');
    this.loadProfile(this.profile.id);

    this.loadAllFrinds();
    this.checkButton();
    if (this.profile.id === this.profileId.id) {
      this.btns = false;
    }
  }

  loadProfile(id: number) {
    this.profileService.findID(id).subscribe((profile) => {
      this.profile = profile;
      this.getPictureProfile(this.profile);

    })
  }

  loadAllFrinds() {
    this.profileService.getFriends(this.friendsId.id).subscribe((res: ResponseWrapper) => {
      this.profiles = res.json;
      this.loadPictureFriends();
    })
  }

  addFriends() {
    this.profileService.createFriends(this.profileId.id, this.friendsId.id, this.confirm).subscribe((value) => {
      console.log(value);
      if (value.statusText == "OK") {
        let alert = this.alertCtrl.create({
          title: 'Запрос отправлен', buttons: ['OK']
        });
        alert.present();

        this.checkButton();

        this.navCtrl.pop();
      }
    }, err => {
      console.log(err);
      let alert = this.alertCtrl.create({
        title: 'Вы уже отправляли запрос', buttons: ['OK']
      });
      alert.present();
    });
  }

  deleteFriends(friendsId: Profile) {
    this.profileService.deleteFriendship(this.profileId.id, friendsId.id).subscribe((value) => {
      if (value.statusText == "OK") {
        this.profileService.deleteFriendship(friendsId.id, this.profileId.id).subscribe((value2 => {
          if (value2.statusText == "OK") {
            this.navCtrl.push('ProfilePage');
          }
        }));
      }
    });
  }

  checkButton() {
    this.openProfileService.getFriendsRequest(this.profileId.id, this.friendsId.id).subscribe((value) => {
      console.log(value);
       if (value === "false") {
        this.btn = true;
        } else if (value === "true") {
        this.btn = false;
      }
    });
  }

  private getPictureProfile(profile: Profile) {
    return this.friellengeService.loadPicture(profile.linkPhoto).subscribe((res) => {
      profile.linkPhoto = 'data:image/jpeg;base64,' + res.text();
    });
  }

  private loadPictureFriends() {
    for (let entry of this.profiles) {
      this.getPictureProfile(entry);
    }
  }
}

