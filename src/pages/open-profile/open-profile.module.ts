import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OpenProfilePage } from './open-profile';


@NgModule({
  declarations: [
    OpenProfilePage,
  ],
  imports: [
    IonicPageModule.forChild(OpenProfilePage),

  ],
})
export class OpenProfilePageModule {}
