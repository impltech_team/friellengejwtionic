import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import {Facebook} from '@ionic-native/facebook';
import {MyApp} from "../../app/app.component";


/**
 * The Welcome Page is a splash page that quickly describes the app,
 * and then directs the user to create an account or log in.
 * If you'd like to immediately put the user onto a login/signup page,
 * we recommend not using the Welcome page.
*/
@IonicPage()
@Component({
  selector: 'page-welcome',
  templateUrl: 'welcome.html'
})

export class WelcomePage {

  isUserLoggedIn: any = false;
  userInfo: any = {};
  token:any;
  auth:any;

  constructor(public navCtrl: NavController, public fb: Facebook, public myapp:MyApp) { }

  loginWithFB() {

    this.fb.login(['public_profile', 'email','user_location'])
      .then(loginRes => {
        this.fb.api('me/?fields=id,email,name,picture,location', ['public_profile', 'email', 'user_location'])
          .then(apiRes => {
            this.userInfo = apiRes;
            localStorage.setItem("FB_ID", loginRes.authResponse.userID);
            localStorage.setItem("FB_token", loginRes.authResponse.accessToken);
            this.isUserLoggedIn = true;
            this.token = JSON.stringify(this.fb.getAccessToken());
            this.auth = JSON.stringify(loginRes.authResponse);
            console.log('Token: ', this.fb.getAccessToken());
            console.log('Auth: ', loginRes.authResponse);
            console.log('Status: ', loginRes.status);
            console.log('location: ', JSON.stringify(apiRes));
            this.navCtrl.push('LoginPage');
          }).catch(apiErr => console.log('Error api response', apiErr))
      })
      .catch(loginErr => console.log('Error logging into Facebook', loginErr));
  }

  logout() {
    this.fb.logout().then(logoutRes => this.isUserLoggedIn = false)
      .catch(logoutErr => console.log('Error logout', logoutErr));
  }

  login() {
    this.navCtrl.push('LoginPage');
  }

  // signup() {
  //   this.navCtrl.push('SignupPage');
  // }
}
