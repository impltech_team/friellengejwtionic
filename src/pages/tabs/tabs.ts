import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import {Events, IonicPage, MenuController, NavController, NavParams, Tab} from 'ionic-angular';
import {Tab1Root, Tab2Root, Tab3Root, Tab4Root, Tab5Root, Tab6Root} from '../pages';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html'
})
export class TabsPage {
  tab1Root: any = Tab1Root;
  tab2Root: any = Tab2Root;
  tab3Root: any = Tab3Root;
  tab4Root: any = Tab4Root;
  tab5Root: any = Tab5Root;
  tab6Root: any = Tab6Root;
  myIndex: number;

  tab1Title = " ";
  tab2Title = " ";
  tab3Title = " ";
  tab4Title = " ";
  tab5Title = " ";
  tab6Title = " ";

  constructor(public navCtrl: NavController,
              public translateService: TranslateService,
              public navParams: NavParams,
              public events: Events,
              public menuCtrl: MenuController
  ) {
    translateService.get(['TAB1_TITLE', 'TAB2_TITLE', 'TAB3_TITLE', 'TAB4_TITLE','TAB5_TITLE', 'TAB6_TITLE']).subscribe(values => {
      this.tab1Title = values['TAB1_TITLE'];
      this.tab2Title = values['TAB2_TITLE'];
      this.tab3Title = values['TAB3_TITLE'];
      this.tab4Title = values['TAB4_TITLE'];
      this.tab5Title = values['TAB5_TITLE'];
      this.tab6Title = values['TAB6_TITLE'];

      this.myIndex = navParams.data.tabIndex || 0;
    });
  }

  tabSelected(tab: Tab) {
    if(tab.index == 6){
      this.openMenu();
    }else{
      console.log("TAB INDEX ",tab.index);
      console.log('publish loadAll');
      this.events.publish('loadAll',tab.index);
    }
  }

  openMenu() {
    this.menuCtrl.open();
  }

}
