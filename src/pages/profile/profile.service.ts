import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions, Response} from '@angular/http';
import {Observable} from 'rxjs/Rx';
import {ResponseWrapper} from "../../models/response-wrapper.model";
import {createRequestOption} from "../../models/request-util";
import {Profile} from "./profile.model";
import {Api} from "../../providers/api/api";
import {FriendsModel} from "../friends/friends.model";


@Injectable()
export class ProfileService {

  private resourceUrls = 'http://' + Api.SERVER_URL + '/api/profilesfb';
  private resourceUrlsID = 'http://' + Api.SERVER_URL + '/api/profiles';
  private resourceUrlsFriends = 'http://' + Api.SERVER_URL + '/api/friends';
  private resourceUrlsGetFriends = 'http://' + Api.SERVER_URL + '/api/friendsByProfile';
  private resourceUrlsGetFriendsRequest = 'http://' + Api.SERVER_URL + '/api/friendsByProfileRequest';
  private resourceUrlDeleteFriends = 'http://' + Api.SERVER_URL + '/api/friends';
  private resourceUrlAddFriends = 'http://' + Api.SERVER_URL + '/api/friendsUpd';

  constructor(private http: Http) {
  }

  query(req?: any): Observable<ResponseWrapper> {
    const options = createRequestOption(req);
    return this.http.get(this.resourceUrls, options)
      .map((res: Response) => this.convertResponse(res));
  }

  private convertResponse(res: Response): ResponseWrapper {
    const jsonResponse = res.json();
    const result = [];
    for (let i = 0; i < jsonResponse.length; i++) {
      result.push(this.convertItemFromServer(jsonResponse[i]));
    }
    return new ResponseWrapper(res.headers, result, res.status);
  }

  private convertItemFromServer(json: any): Profile {
    const entity: Profile = Object.assign(new Profile(), json);
    return entity;
  }

  private convert(profile: Profile): Profile {
    const copy: Profile = Object.assign({}, profile);
    return copy;
  }

  createFriends(profileId: number, friendsId: number, confirm: boolean): Observable<Response> {
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});

    return this.http.post(this.resourceUrlsFriends + "?profileId=" + profileId + "&friendsId=" + friendsId + "&confirm=" + confirm, options);
  }

  find(id: number): Observable<Profile> {
    return this.http.get(`${this.resourceUrls}/${id}`).map((res: Response) => {
      const jsonResponse = res.json();
      return this.convertItemFromServer(jsonResponse);
    });
  }

  findID(id: number): Observable<Profile> {
    return this.http.get(`${this.resourceUrlsID}/${id}`).map((res: Response) => {
      const jsonResponse = res.json();
      return this.convertItemFromServer(jsonResponse);
    });
  }

  getFriends(id: number): Observable<ResponseWrapper> {
    return this.http.get(`${this.resourceUrlsGetFriends}/${id}`).map((res: Response) => this.convertResponse(res));
  }

  getFriendsRequest(id: number): Observable<ResponseWrapper> {
    return this.http.get(`${this.resourceUrlsGetFriendsRequest}/${id}`).map((res: Response) => this.convertResponse(res));
  }

  update(profile: Profile): Observable<Profile> {
    const copy = this.convert(profile);
    return this.http.put(this.resourceUrlsID, copy).map(res => res.text() ? res.json() : res);
  }


  deleteFriendship(profileId: number, friendsId: number): Observable<Response> {
    return this.http.delete(this.resourceUrlDeleteFriends + "?profileId=" + profileId + "&friendsId=" + friendsId);
  }

  updateFriends(friends: FriendsModel): Observable<Response> {
    let body = JSON.stringify(friends);
    let headers = new Headers({'Content-Type': 'application/json'});
    let options = new RequestOptions({headers: headers});
    return this.http.put(this.resourceUrlAddFriends, body, options);
  }
}
