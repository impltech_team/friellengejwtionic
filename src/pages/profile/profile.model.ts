import {BaseEntity} from "../../models";

export class Profile implements BaseEntity{
  constructor(
    public id?: number,
    public fullName?: string,
    public linkPhoto?: string,
    public userDonat?: number,
    public facebookId?: number,
    public token?: string,
    public location?: string,
    public description?: string,
    public likesAuthor?: number,
    public likesUser?: number,
    public isInvite?: boolean
  ) {
  }
}

