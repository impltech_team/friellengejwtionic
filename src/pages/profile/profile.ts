import {Component, OnInit} from '@angular/core';
import {IonicPage, NavController, NavParams, PopoverController, Tab, ToastController} from 'ionic-angular';
import {Profile} from "./profile.model";
import {ProfileService} from "./profile.service";
import {ResponseWrapper} from "../../models/response-wrapper.model";
import {Events} from 'ionic-angular';
import {AlertController} from 'ionic-angular';
import {FriendsModel} from "../friends/friends.model";
import {ApprovalModel} from "../approval/approval.model";
import {FriellengeService} from "../friellenge/friellenge.service";
import {PopnewfriComponent} from "../../components/popnewfri/popnewfri";
import {Camera, CameraOptions} from "@ionic-native/camera";
import {Response} from "@angular/http";


@IonicPage()
@Component({
  selector: 'page-profile', templateUrl: 'profile.html',
})


export class ProfilePage implements OnInit {

  profiles: Profile[];

  profilesRequest: Profile[];

  profile: Profile;

  newFriends: FriendsModel = new FriendsModel(null);

  id: any = localStorage.getItem("FB_ID");
  // id: any = 1114912548651106;

  profileId = JSON.parse(localStorage.getItem("profile"));
  friendsId = this.navParams.get('profile');

  imageData: any;

  myphoto: any;


  ngOnInit() {
    this.loadProfile(this.id);
    this.loadAllFrinds();
    this.loadAllFrindsRequest();
  }

  constructor(public events: Events, public navCtrl: NavController,
              public navParams: NavParams, private profileService: ProfileService,
              public alertCtrl: AlertController,
              private camera: Camera,
              public popoverCtrl: PopoverController,
              public friellengeService: FriellengeService,
              public toast: ToastController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ProfilePage');
  }

  openProfile(profile: Profile) {
    this.navCtrl.push('OpenProfilePage', {profile: profile})
  }

  loadAllProfile() {
    this.profileService.query().subscribe((res: ResponseWrapper) => {
      this.profiles = res.json;
    });
  }

  loadAllFrinds() {
    this.profileService.getFriends(this.profileId.id).subscribe((res: ResponseWrapper) => {
      this.profiles = res.json;
      this.loadPictureFriends();
    })
  }

  loadAllFrindsRequest() {
    this.profileService.getFriendsRequest(this.profileId.id).subscribe((res: ResponseWrapper) => {
      this.profilesRequest = res.json;
      this.loadPictureFriendsRequest();
    })
  }


  loadProfile(id: number) {

    this.profileService.find(id).subscribe((res: Profile) => {
      // console.log('profile page', res);
      // this.profile = res;
      localStorage.setItem("profile", JSON.stringify(res));
      this.profile = JSON.parse(localStorage.getItem("profile"));
      this.getPictureProfile(this.profile);
      // this.loadPicture();
      console.log('profile page');
      // console.log(this.profile.location);
      // console.log(this.profile.linkPhoto);
      // this.events.publish('profile', this.profile);
    }, err => {
      console.log("error load profile ", err);
    }, () => {
    });
  }

  ignoreRequestFriendship(friendsId: Profile) {
    this.profileService.deleteFriendship(friendsId.id, this.profileId.id).subscribe((value) => {
      if (value.statusText == "OK") {
        this.loadAllFrindsRequest();
      }
    });
  }

  confirmFriends(friendsId: Profile) {

    this.newFriends.profile = friendsId;
    this.newFriends.friends = this.profileId;
    this.newFriends.confirmation = true;

    this.profileService.updateFriends(this.newFriends).subscribe((value) => {
      if (value.statusText == "OK") {
        this.profileService.createFriends(this.profileId.id, friendsId.id, true).subscribe((value2 => {
          if (value2.statusText == "OK") {
            this.loadAllFrinds();
            this.loadAllFrindsRequest();
          }
        }));
      }
    });
  }


  showPrompt() {
    let prompt = this.alertCtrl.create({
      title: 'Edit data',

      inputs: [{
        name: 'fullName', placeholder: 'Your name'
      }, {
        name: 'location', placeholder: 'Place'
      }, {
        name: 'description', placeholder: 'Slogon'
      }
      ],
      buttons: [{
        text: 'Cancel', handler: data => {
          console.log('Cancel clicked');
        }
      }, {
        text: 'Save', handler: data => {
          if (data.fullName != "") {
            this.profile.fullName = data.fullName;
          }
          if (data.location != "") {
            this.profile.location = data.location;
          }
          if (data.description != "") {
            this.profile.description = data.description;
          }
          if (data.linkPhoto != "") {
            this.friellengeService.savePicture(this.imageData).subscribe((res: Response) => {
              if (res.statusText == 'OK') {
                this.profile.linkPhoto = res.text();
              }
            })
            // this.profile.linkPhoto = data.linkPhoto;
          }
          this.profileService.update(this.profile).subscribe((profile) => {
            this.profile = profile;
            this.loadProfile(this.id);
          })
        }
      }]
    });
    prompt.present();
  }


  private getPictureProfile(profile: Profile) {
    return this.friellengeService.loadPicture(profile.linkPhoto).subscribe((res) => {
      profile.linkPhoto = 'data:image/jpeg;base64,' + res.text();
    });
  }

  private loadPictureFriends() {
    for (let entry of this.profiles) {
      this.getPictureProfile(entry);
    }
  }

  private loadPictureFriendsRequest() {
    for (let entry of this.profilesRequest) {
      this.getPictureProfile(entry);
    }
  }

  takePhoto() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true,
      correctOrientation: true,
      allowEdit: true
    };

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.imageData = imageData;
      this.friellengeService.savePicture(this.imageData).subscribe((res: Response) => {
        if (res.statusText == 'OK') {
          this.profile.linkPhoto = res.text();
          this.profileService.update(this.profile).subscribe((profile) => {
            this.profile = profile;
            this.loadProfile(Number(localStorage.getItem("FB_ID")));
            this.getPictureProfile(this.profile);
          })
        }
      });
      this.myphoto = 'data:image/jpeg;base64,' + imageData;
      this.profile.linkPhoto = this.myphoto;
    }, (err) => {
      console.log(err);
      this.showToast("Ошибка сервера");
    });
  }

  getImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      allowEdit: true
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.imageData = imageData;
      this.friellengeService.savePicture(this.imageData).subscribe((res: Response) => {
        if (res.statusText == 'OK') {
          this.profile.linkPhoto = res.text();
          this.profileService.update(this.profile).subscribe((profile) => {
            this.profile = profile;
            this.loadProfile(Number(localStorage.getItem("FB_ID")));
            this.getPictureProfile(this.profile);
          })
        }
      });
      this.myphoto = 'data:image/jpeg;base64,' + imageData;
      this.profile.linkPhoto = this.myphoto;

    }, (err) => {
      console.log(err);
      this.showToast("Ошибка сервера");
    });
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopnewfriComponent);
    popover.present({
      ev: myEvent
    });

    popover.onDidDismiss(popoverData => {
      if (popoverData !== null) {
        if (popoverData.item == 'Галерея')
          this.getImage();
        else if (popoverData.item == 'Камера')
          this.takePhoto();
      }
    });
  }

  showToast(name: string) {
    const toast = this.toast.create({
      message: `${name}`,
      duration: 2000
    });
    toast.present();
  }
}

