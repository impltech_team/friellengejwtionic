import {BaseEntity} from "../../models";
import {Profile} from "../profile/profile.model";


export class FriendsModel implements BaseEntity {
  constructor(
    public id?: number,
    public profile?: Profile,
    public friends?: Profile,
    public confirmation?: boolean,

  ) {
  }
}
