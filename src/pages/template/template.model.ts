import {BaseEntity} from "../../models";

export class Template implements BaseEntity {
  constructor(public id?: number,
              public description?: string,
              public contentReference?: string,
              public nameOfTemplate?: string,
              public typeOfTemplate?: string,)
  {

  }

}
