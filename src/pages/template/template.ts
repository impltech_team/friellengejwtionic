import {Component, OnDestroy, OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {Template} from "./template.model";
import {TemplateServiceProvider} from "../../providers/template-service/template-service";
import {ResponseWrapper} from "../../models/response-wrapper.model";

@IonicPage()
@Component({
  selector: 'page-template',
  templateUrl: 'template.html',
})
export class TemplatePage implements OnInit, OnDestroy {
  template: Template[];
  typeOfTemplate:any;

  ngOnInit() {
    this.loadAll();
  }
  ngOnDestroy() {
  }

  constructor(public navCtrl: NavController, public navParams: NavParams,
              private templateService: TemplateServiceProvider) {
    this.typeOfTemplate = navParams.get('typeOfTemplate');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TemplatePage');
  }

   loadAll() { //
    this.templateService.query(this.typeOfTemplate).subscribe(
      (res: ResponseWrapper) => {
        this.template = res.json;
      },
    );
  }

  choose(templateItem: number){
    this.navCtrl.push('NewFriellengePage', {templateItem: templateItem});
  }
}
