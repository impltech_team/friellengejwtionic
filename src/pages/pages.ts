// The page the user lands on after opening the app and without a session
// import {OpenFriellengePage} from "./open-friellenge/open-friellenge";
// import {FriellengePage} from "./friellenge/friellenge";

import {MyApprovalsPage} from "./my-approvals/my-approvals";

export const FirstRunPage = 'WelcomePage';

// The main page the user will see as they use the app over a long periodForEnd of time.
// Change this if not using tabs
export const MainPage = 'TabsPage';

// The initial root pages for our tabs (remove if not using tabs)
export const Tab1Root = 'FriellengePage';
export const Tab2Root = 'FriellengePage';
export const Tab3Root = 'FriellengePage';
export const Tab4Root = 'FriellengePage';
export const Tab5Root = 'ProfilePage';
export const Tab6Root = 'MyApprovalsPage';

// export const Friellenge = 'FriellengePage';
