import {Component, OnDestroy, OnInit} from '@angular/core';
import {
  ActionSheetController, AlertController, IonicPage, NavController, NavParams, Platform,
  PopoverController, ToastController
} from 'ionic-angular';
import {ResponseWrapper} from "../../models/response-wrapper.model";
import {Friellenge} from "./friellenge.model";
import {FriellengeService} from "./friellenge.service";
import {Profile} from "../profile/profile.model";
import {ApprovalServiceProvider} from "../../providers/approval-service/approval-service";
import {ApprovalModel} from "../approval/approval.model";
import {Location} from '@angular/common';
import {Events} from 'ionic-angular';
import {SimpleTimerPage} from "../simple-timer/simple-timer";
import {OpenProfilePage} from "../open-profile/open-profile";
import {PopMenuOfFriComponent} from "../../components/pop-menu-of-fri/pop-menu-of-fri";
import {PopMenuOfFriAuthorComponent} from "../../components/pop-menu-of-fri-author/pop-menu-of-fri-author";
import {ReportService} from "../report/report.service";
import {isNullOrUndefined} from "util";
import { Base64ToGallery } from '@ionic-native/base64-to-gallery';
import {CommentModel} from "../comment/comment.model";

// import {SocialSharing} from "@ionic-native/social-sharing";
// import {Http} from "@angular/http";

/**
 * Generated class for the FriellengePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-friellenge',
  templateUrl: 'friellenge.html',
})
export class FriellengePage implements OnInit, OnDestroy {

  friellengeLine: Friellenge[] = [];
  timeInSeconds: number;
  remainingSeconds: number;
  runTimer: boolean;
  hasStarted: boolean;
  hasFinished: boolean;
  displayTime: string;
  profile: Profile;
  participate: any[];
  approval: ApprovalModel;
  periodForEnd = {days: 0, wholeDays: 0, hours: 0, wholeHours: 0, minutes: 0, wholeMinutes: 0, seconds: 0};
  periodForStatr = {days: 0, wholeDays: 0, hours: 0, wholeHours: 0, minutes: 0, wholeMinutes: 0, seconds: 0};
  descending: boolean = false;
  order: number;
  column: string = 'id';
  subcolumn: string = '';
  tab: number = 0;
  search: boolean = false;

  // newReport: ReportModel = new ReportModel(null);

  constructor(public popoverCtrl: PopoverController,
              private location: Location,
              public approvalService: ApprovalServiceProvider,
              public navCtrl: NavController,
              public navParams: NavParams,
              private alertCtrl: AlertController,
              private friellengeService: FriellengeService,
              public platform: Platform,
              public events: Events,
              public toast: ToastController,
              public actionsheetCtrl: ActionSheetController,
              public reportService: ReportService,
              private base64ToGallery: Base64ToGallery) {

  }

  // ngOnChanges() {
  //   this.participateProfile();
  // }

  ngOnInit() {
    this.profile = JSON.parse(localStorage.getItem("profile"));
    // this.principal.identity().then((account) => {
    //   this.currentAccount = account;
    // });
    // this.registerChangeInOrders();
    // this.friellengeLine.sort();
    // this.commentPage(friellengeId: number);
     this.loadAll();
  }

  subscribeAll(){
    this.events.subscribe('loadAll', (value) => {
      console.log('subscribe reloadall',value);
      if(isNullOrUndefined(value) == true){
        console.log('value undef');
        this.loadAll();
      }else{
        console.log('value def');
        this.tab=value;
        this.loadAll();
      }
    });
  }

  unSubscribeAll(){
    this.events.unsubscribe('loadAll');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FriellengePage');
  }

  ngOnDestroy() {
    console.log("Destroy fril");
  }

  load() {
    location.reload()
  }

  openFriellenge(friellenge: Friellenge) {
    this.navCtrl.push('OpenFriellengePage', {friellenge: friellenge})
  }

  approvalFriellenge(friellenge: Friellenge) {
    this.navCtrl.push('ApprovalPage', {friellenge: friellenge})
  }

  openProfile(profile: Profile) {
    this.navCtrl.push('OpenProfilePage', {profile: profile})
  }

  addLike(friellengeId: number) {
    for (let entry of this.friellengeLine) {
      if (entry.id == friellengeId && entry.isLiked) {
        entry.friellengeLikes--;
        entry.isLiked = false;
        this.friellengeService.deleteLikeByFriellengeIdAndProfileId(entry.id, this.profile.id).subscribe();
        this.friellengeService.update(entry).subscribe(value => {
          if (value.statusText !== "OK") {
            entry.friellengeLikes++;
            entry.isLiked = true;
          }
        });
        break;
      }
      else if (entry.id == friellengeId && (!entry.isLiked)) {
        entry.friellengeLikes++;
        entry.isLiked = true;
        this.friellengeService.addLikeByFriellengeIdAndProfileId(entry.id, this.profile.id).subscribe();
        this.friellengeService.update(entry).subscribe(value => {
          if (value.statusText !== "OK") {
            entry.friellengeLikes--;
            entry.isLiked = false;
          }
        });
        break;
      }
    }
  }

  addToFavorite(friellengeId: number) {
    for (let entry of this.friellengeLine) {
      if (entry.id == friellengeId && entry.isFavorite) {
        entry.isFavorite = false;
        this.friellengeService.deleteFavoriteByFriellengeIdAndProfileId(entry.id, this.profile.id).subscribe();
        this.friellengeService.update(entry).subscribe(value => {
          if (value.statusText !== "OK") {
            entry.isFavorite = true;
          }
        });
        break;
      }
      else if (entry.id == friellengeId && (!entry.isFavorite)) {
        entry.isFavorite = true;
        this.friellengeService.addFavoriteByFriellengeIdAndProfileId(entry.id, this.profile.id).subscribe();
        this.friellengeService.update(entry).subscribe(value => {
          if (value.statusText !== "OK") {
            entry.isLiked = false;
          }
        });
        break;
      }
    }
  }

  commentPage(friellenge: Friellenge) {
    this.navCtrl.push('CommentPage', {friellenge: friellenge});
  }

  loadAll() {
    this.unSubscribeAll();
    switch (this.tab) {
      case 0: {
        console.log("Select All");
        this.loadAllFriellenges();
        break;
      }
      case 1: {
        console.log("Select Active");
        this.loadAllActive();
        break;
      }
      case 2: {
        console.log("Select Favourite");
        this.loadAllFavourite();
        break;
      }
      case 3: {
        console.log("Select My");
        this.loadAllMy();
        break;
      }
      default: {
        console.log("Invalid choice load");
        break;
      }
    }
  }

  loadAllFriellenges() {
    this.friellengeService.findAllFriellengesByIdProfile(this.profile.id).subscribe(
      (res: ResponseWrapper) => {
        this.friellengeLine = res.json;
      },
      (res: ResponseWrapper) => {
        console.log(res.json)
      }, () => {
        this.participateProfile();
        this.getDate();
        this.sort(false, 'id');
        this.loadPicture();
        this.loadPictureProfile();
      });
    console.log('load All Friellenges');
    this.subscribeAll();
  }

  loadAllActive() {
    this.friellengeService.findAllActiveByIdProfile(this.profile.id).subscribe(
      (res: ResponseWrapper) => {
        this.friellengeLine = res.json;
      },
      (res: ResponseWrapper) => {
        console.log(res.json)
      }, () => {
        this.participateProfile();
        this.getDate();
        this.sort(false, 'lastApproval');
        this.loadPicture();
        this.loadPictureProfile();
      });
    console.log('load All Active');
    this.subscribeAll();
  }

  loadAllFavourite() {

    this.friellengeService.findAllFavouriteByIdProfile(this.profile.id).subscribe(
      (res: ResponseWrapper) => {
        this.friellengeLine = res.json;
      },
      (res: ResponseWrapper) => {
        console.log(res.json)
      }, () => {
        this.participateProfile();
        this.getDate();
        this.sort(false, 'id');
        this.loadPicture();
        this.loadPictureProfile();
      });
    console.log('load All Favourite');
    this.subscribeAll();
  }

  loadAllMy() {
    this.friellengeService.findAllMyByIdProfile(this.profile.id).subscribe(
      (res: ResponseWrapper) => {
        this.friellengeLine = res.json;
      },
      (res: ResponseWrapper) => {
        console.log(res.json)
      }, () => {
        this.participateProfile();
        this.getDate();
        this.sort(false, 'id');
        this.loadPicture();
        this.loadPictureProfile();
      });
    console.log('load All My');
    this.subscribeAll();
  }

  newfri() {
    this.navCtrl.push('NewFriellengePage');
  }

  simpleTimer() {
    this.navCtrl.push(SimpleTimerPage);
  }

  openMenu() {
    let actionSheet = this.actionsheetCtrl.create({
      title: 'Кнопка сортировок',
      cssClass: 'action-sheets-basic-page',
      buttons: [
        {
          text: 'Популярные',
          role: 'destructive',
          icon: !this.platform.is('ios') ? 'trash' : null,
          handler: () => {
            console.log('Delete clicked');
            this.sortByPopular();
          }
        },
        {
          text: 'Лучшие',
          icon: !this.platform.is('ios') ? 'share' : null,
          handler: () => {
            console.log('Share clicked');
            this.sortByBest();
          }
        },
        {
          text: 'Звезды фрэленджей',
          icon: !this.platform.is('ios') ? 'arrow-dropright-circle' : null,
          handler: () => {
            console.log('Play clicked');
          }
        },
        {
          text: 'Поиск',
          icon: !this.platform.is('ios') ? 'heart-outline' : null,
          handler: () => {
            console.log('Favorite clicked');
            this.search=true;
          }
        },
        {
          text: 'Cancel',
          role: 'cancel', // will always sort to be on the bottom
          icon: !this.platform.is('ios') ? 'close' : null,
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }

  sortByBest(){
    console.log('Sorting by best');
    this.sort(false, 'friellengeLikes');
}

  sortByPopular(){
    console.log('Sorting by Popular');
    this.sort(false, 'countOfProfileByFriellenge');
  }

  participateProfile() {
    this.friellengeService.getParticipateProfile(this.profile.id).subscribe((res: any) => {
      this.participate = res;
      // loop1:
      for (let id of this.participate) {
        console.log("id participate ", id);
        for (let friell of this.friellengeLine) {
          // console.log("this.friellenge.id", friell.id);
          console.log("check participate ", friell);
          if (id == friell.id) {
            friell.isJoinIn = true;
            console.log("participate fri ", friell.isJoinIn);
            break;
          }
        }
      }
    })
  }

  participateProfile1(friellenge: Friellenge) {
    this.friellengeService.getParticipateProfile(this.profile.id).subscribe((res: any) => {
      this.participate = res;
      loop1:
        for (let id of this.participate) {
          console.log("id", id);
          for (let friell of this.friellengeLine) {
            // console.log("this.friellenge.id", friell.id);
            console.log(friell);
            if (id == friell.id) {
              friell.isJoinIn = !friell.isJoinIn;
              console.log(friell.isJoinIn);
              break loop1;
            }
          }
        }
    })
  }

  doRefresh(refresher) {
    this.loadAll();
    setTimeout(() => {
      refresher.complete();
    }, 2000);
  }

  // isJoinIn(friellenge: Friellenge) {
  //     for (let id of this.participate) {
  //       if (id == friellenge.id) {
  //         return true
  //       }
  //     }
  // }

  leaveIt(friellenge: Friellenge) {
    this.approvalService.deleteApprovalsByFriellengeIdAndProfileId(friellenge.id, this.profile.id).subscribe(
      () => {
        friellenge.isJoinIn = false;
        friellenge.countOfProfileByFriellenge--;
      },
      (res: ResponseWrapper) => {
        console.log("Status ERROR", res);
        this.showToast("Ошибка сервера");
      },
      () => {
        this.loadAll();
      }
    );
    // this.participateProfile();
  }

  joinIn(friellenge: Friellenge) {

    console.log("JOIN ", friellenge);
    this.approval = new ApprovalModel(null);
    this.approval.profile = new Profile();
    this.approval.friellenge = new Friellenge();
    this.approval.profile.id = this.profile.id;
    this.approval.friellenge.id = friellenge.id;
    this.approval.comment = '';
    this.approval.reference = '';
    this.approval.confirmation = false;
    this.approvalService.addNewApproval(this.approval).subscribe(
      () => {
        friellenge.isJoinIn = true;
        friellenge.countOfProfileByFriellenge++;
      },
      (res: ResponseWrapper) => {
        console.log("Status ERROR", res);
        this.showToast("Ошибка сервера");
      },
      () => {
        this.loadAll();
      }
    );
    // this.participateProfile();
  }


  getDate() {
    console.log("getdate ", this.friellengeLine);

    for (let frie of this.friellengeLine) {
      let endDate = frie.endDate;
      let startDate = frie.startDate;

      this.calculusDate(endDate, this.periodForEnd);
      this.calculusDate(startDate, this.periodForStatr);
      // this.calculusDate(startDate);


      if (frie.status === 'Finished') {
        frie.remains = 'Finished'
      }
      else if (frie.status === 'Deffered') {
        frie.remains = ("Days for start " + this.periodForStatr.wholeDays.toLocaleString());
      }

      else if (this.periodForEnd.wholeDays >= 1 && frie.status === 'InProcess') {
        frie.remains = ("Days of friellenge " + (this.periodForEnd.wholeDays + 1).toLocaleString());
      }

      else if (this.periodForEnd.wholeDays == 0 && this.periodForEnd.wholeHours >= 1 && frie.status === 'InProcess') {
        console.log('More than month');
        frie.remains = ("Hours of friellenge " + this.periodForEnd.wholeHours.toLocaleString());
      }

      else if (this.periodForEnd.wholeDays == 0 && this.periodForEnd.wholeHours == 0, this.periodForEnd.wholeMinutes > 5 && frie.status === 'InProcess') {
        console.log('More than month');
        frie.remains = ("Minutes of friellenge " + this.periodForEnd.wholeMinutes.toLocaleString());
      }

      else if (this.periodForEnd.minutes <= 5 && frie.status === 'InProcess') {
        console.log("second " + this.periodForEnd.seconds);
        console.log("min " + this.periodForEnd.minutes);
        console.log("sec + mim " + ((this.periodForEnd.wholeMinutes * 60) + this.periodForEnd.seconds));

        this.setTime((this.periodForEnd.wholeMinutes * 60) + this.periodForEnd.seconds);
        this.timers(frie);

        frie.remains = "Time to end";
        // frie.remains = this.getSecondsAsDigitalClock(this.periodForEnd.seconds);
      }
      else {
        console.log('Some error');
        frie.remains = 'Some error';
      }
      console.log(frie.remains);
    }
  }

  getSecondsAsDigitalClock(inputSeconds: number) {
    var sec_num = parseInt(inputSeconds.toString(), 10);
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);
    var hoursString = '';
    var minutesString = '';
    var secondsString = '';
    // hoursString = (hours < 10) ? "0" + hours : hours.toString();
    minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
    secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
    return minutesString + ':' + secondsString;
  }

  calculusDate(date: Date, period: any) {
    let time = new Date(date).getTime() - new Date().getTime();
    period.days = time / 1000 / 60 / 60 / 24;
    period.wholeDays = Math.floor(period.days);
    period.hours = (period.days - period.wholeDays) * 24;
    period.wholeHours = Math.floor(period.hours);
    period.minutes = (period.hours - period.wholeHours) * 60;
    period.wholeMinutes = Math.floor(period.minutes);
    period.seconds = Math.floor((period.minutes - period.wholeMinutes) * 60);

    // console.log(period.wholeDays + ' wholeDays');
    // console.log(period.days + ' days');
    // console.log(period.hours + ' hours');
    // console.log(period.wholeHours + ' wholeHours');
    // console.log(period.minutes + ' minutes');
    // console.log(period.wholeHours + ' wholeMinutes');
    // console.log(period.wholeMinutes + ' seconds');

  }

  timers(frie: Friellenge) {

    setTimeout(() => {
      this.startTimer(frie);
    }, 1000);

    // this.timeInSeconds=300;
    this.initTimer();
  }

  initTimer() {
    if (!this.timeInSeconds) {
      this.timeInSeconds = 0;
    }

    this.runTimer = false;
    this.hasStarted = false;
    this.hasFinished = false;
    this.remainingSeconds = this.timeInSeconds;
    this.displayTime = this.getSecondsAsDigitalClock(this.remainingSeconds);
  }

  startTimer(frie: Friellenge) {
    this.runTimer = true;
    this.hasStarted = true;
    this.timerTick(frie);
  }

  timerTick(frie: Friellenge) {
    setTimeout(() => {
      if (!this.runTimer) {
        return;
      }
      this.remainingSeconds--;
      this.displayTime = this.getSecondsAsDigitalClock(this.remainingSeconds);
      if (this.remainingSeconds > 0) {
        this.timerTick(frie);
      }
      else {
        this.hasFinished = true; //todo update friellenge
        frie.status = 'Finished';
      }
    }, 1000);
  }

  setTime(time) {
    this.timeInSeconds = time;

    // this.pauseTimer();
    // this.initTimer();

    // As startTimer() has the this.timerTick(), which we don't want to call here
    // otherwise two seconds will change per tick
    // this.runTimer = true;
    // this.hasStarted = true;
  }

  presentPopover(event, friellenge: Friellenge) {
    let popover;
    if (friellenge.profile.id === this.profile.id) {
      popover = this.popoverCtrl.create(PopMenuOfFriAuthorComponent);
      popover.present({
        ev: event
      });
    }
    else {
      popover = this.popoverCtrl.create(PopMenuOfFriComponent);
      popover.present({
        ev: event
      });
    }
    popover.onDidDismiss(popoverData => {
      if (popoverData != null && popoverData.item == 'Репорт')
        this.doRepost(friellenge, null, null);
      else if (popoverData != null && popoverData.item == 'Донат')
        this.doDonat(friellenge.id);
      else if (popoverData != null && popoverData.item == 'Удалить')
        this.deleteFriellenge(friellenge.id);
    });
  }

  showToast(name: string) {
    const toast = this.toast.create({
      message: `${name}`,
      duration: 2000
    });
    toast.present();
  }

  private deleteFriellenge(friellengeId: number) {
    this.friellengeService.deleteFriellengeById(friellengeId).subscribe(
      (value) => {
        if (value.statusText == "OK") {
          // this.showPopUp("Фриленж удален");
          this.showToast("Фриленж удален");
        }
      },
      (res: ResponseWrapper) => {
        console.log("Status ERROR", res);
        // this.showPopUp("Ошибка сервера");
        this.showToast("Ошибка сервера");
      },
      () => {
        console.log("Status OK and LOAD");
        this.loadAll();
      }
    );

  }

  private doRepost(item: Friellenge,approval: ApprovalModel, comment: CommentModel) { // todo it is need finish this method
        this.reportService.showCheckbox(item, approval, comment);
  }

  private doDonat(friellengeId: number) { // todo it is need finish this method

  }

  sort(descending: boolean, column: string) {
    // this.descending = !this.descending;
    this.descending = descending;
    this.order = this.descending ? 1 : -1;
    this.column = column;
  }

  /**
   * this method return name for picture getting it from reference
   * maybe it will be need for saving picture to folder
   * @param {Friellenge} friellenge
   * @returns {any} - name for picture
   */
  private createNameForPicture(friellenge: Friellenge) {
    let firstIndexOfName = friellenge.reference.lastIndexOf("/");
    console.log("first Index ** " + firstIndexOfName);
    if (firstIndexOfName == null) firstIndexOfName = friellenge.reference.lastIndexOf("\\");
    console.log("first Index ** " + firstIndexOfName);
    let lastIndexOfName = friellenge.reference.lastIndexOf(".");
    console.log("last Index ** " + lastIndexOfName);
    return friellenge.reference.substring(firstIndexOfName + 1, lastIndexOfName);
  }

  /**this method must get tempPicture by friellenge reference and then save it to file but it dont work
   * Error: exec proxy not found for :: Base64ToGallery :: saveImageDataToLibrary
   * Error saving image to gallery  Missing Command Error
   * @param {Friellenge} friellenge
   */
  private getPictureAndSaveToFolder(friellenge: Friellenge) {
      let prefix: any = {
        prefix: 'img_',
        mediaScanner: false
      };
      this.friellengeService.loadPicture(friellenge.reference).subscribe( (res) => {
        friellenge.picture = 'data:image/jpeg;base64,' + res.text();
        this.base64ToGallery.base64ToGallery(friellenge.picture, prefix).then(
          res => console.log('Saved image to gallery ', res),  //Error: exec proxy not found for :: Base64ToGallery :: saveImageDataToLibrary
          err => console.log('Error saving image to gallery ', err) //Error saving image to gallery  Missing Command Error
        );
      });
    }

  private getPictureProfile(profile: Profile) {
    return this.friellengeService.loadPicture(profile.linkPhoto).subscribe( (res) => {
      profile.linkPhoto = 'data:image/jpeg;base64,' + res.text();
    });
  }

  private getPicture(friellenge: Friellenge) {
      return this.friellengeService.loadPicture(friellenge.reference).subscribe( (res) => {
         friellenge.picture = 'data:image/jpeg;base64,' + res.text();
      });
  }

  private loadPicture() {
      for (let entry of this.friellengeLine) {
        this.getPicture(entry);
       }
  }

  private loadPictureProfile() {
    for (let entry of this.friellengeLine) {
      this.getPictureProfile(entry.profile);
    }
  }

}
