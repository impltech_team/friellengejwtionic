import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Rx';
// import { SERVER_API_URL } from '../../app.constants';
import {Friellenge} from "./friellenge.model";
import {ResponseWrapper} from "../../models/response-wrapper.model";
import {createRequestOption} from "../../models/request-util";
import {Headers, RequestOptions} from '@angular/http';
import {Api} from "../../providers/api/api";

// import {log} from "util";

@Injectable()
export class FriellengeService {
  profileId2: any;

  private resourceUrl = 'http://' + Api.SERVER_URL + '/api/friellenges';

  private resourceUrlFindFriellenges = 'http://' + Api.SERVER_URL + '/api/friellengesAll';

  private resourceUrlFindActive = 'http://' + Api.SERVER_URL + '/api/friellengesActive';

  private resourceUrlFindFavourite = 'http://' + Api.SERVER_URL + '/api/friellengesFavourite';

  private resourceUrlFindMy = 'http://' + Api.SERVER_URL + '/api/friellengesMy';

  private resourceUrlFindOne = 'http://' + Api.SERVER_URL + '/api/friellengesFindOne';

  private resourceUrlForLikes = 'http://' + Api.SERVER_URL + '/api/likes';

  private resourceUrlForFavorite = 'http://' + Api.SERVER_URL + '/api/favorites';

  private resourceUrlForParticipate = 'http://' + Api.SERVER_URL + '/api/approvals/friellengeIdByProfile';

  private resourceUrlForPicture = 'http://' + Api.SERVER_URL + '/api/picture';


  constructor(private http: Http) { }

  // create(orders: Friellenge): Observable<Friellenge> {
  //   const copy = this.convert(orders);
  //   return this.http.post(this.resourceUrl, copy).map((res: Response) => {
  //     const jsonResponse = res.json();
  //     return this.convertItemFromServer(jsonResponse);
  //   });
  // }

  create(friellenge: Friellenge): Observable<Response>{
      let body = JSON.stringify(friellenge);
      let headers = new Headers({ 'Content-Type': 'application/json' });
      let options = new RequestOptions({ headers: headers });
      return this.http.post(this.resourceUrl, body, options);
  }

  update(friellenge: Friellenge): Observable<Response> {
    const copy = this.convert(friellenge);
    return this.http.put(this.resourceUrl, copy).map((res: Response) => {
      return res;
    });
  }

  deleteFriellengeById(friellengeId: number): Observable<Response> {
    return this.http.delete(this.resourceUrl +"/" + friellengeId);

  }

  find(id: number): Observable<Friellenge> {
    return this.http.get(`${this.resourceUrl}/${id}`).map((res: Response) => {
      const jsonResponse = res.json();
      return this.convertItemFromServer(jsonResponse);
    });
  }

  findOne(id: number): Observable<Friellenge> {
    return this.http.get(`${this.resourceUrlFindOne}/${id}`).map((res: Response) => {
      const jsonResponse = res.json();
      return this.convertItemFromServer(jsonResponse);
    });
  }

  findAllFriellengesByIdProfile(profileId?: number): Observable<ResponseWrapper> {
    const options = createRequestOption(profileId);
    return this.http.get((this.resourceUrlFindFriellenges + "?profileId=" + profileId), options)
      .map((res: Response) => this.convertResponse(res));
  }

  findAllActiveByIdProfile(profileId?: number): Observable<ResponseWrapper> {
    const options = createRequestOption(profileId);
    return this.http.get((this.resourceUrlFindActive + "?profileId=" + profileId), options)
      .map((res: Response) => this.convertResponse(res));
  }

  findAllFavouriteByIdProfile(profileId?: number): Observable<ResponseWrapper> {
    const options = createRequestOption(profileId);
    return this.http.get((this.resourceUrlFindFavourite + "?profileId=" + profileId), options)
      .map((res: Response) => this.convertResponse(res));
  }

  findAllMyByIdProfile(profileId?: number): Observable<ResponseWrapper> {
    const options = createRequestOption(profileId);
    return this.http.get((this.resourceUrlFindMy + "?profileId=" + profileId), options)
      .map((res: Response) => this.convertResponse(res));
  }

  // delete(id: number): Observable<Response> {
  //   return this.http.delete(`${this.resourceUrl}/${id}`);
  // }
  ////
  deleteLikeByFriellengeIdAndProfileId(friellengeId: number, profileId: number): Observable<Response> {
    console.log(friellengeId + " " + profileId);
    return this.http.delete(this.resourceUrlForLikes +"?friellengeId=" + friellengeId + "&profileId=" + profileId);
  }
  addLikeByFriellengeIdAndProfileId(friellengeId: number, profileId: number) : Observable<Response> {
    console.log(friellengeId + " " + profileId);
    return this.http.post((this.resourceUrlForLikes +"?friellengeId=" + friellengeId + "&profileId=" + profileId), null);
  }

  private convertResponse(res: Response): ResponseWrapper {
    const jsonResponse = res.json();
    const result = [];
    for (let i = 0; i < jsonResponse.length; i++) {
      result.push(this.convertItemFromServer(jsonResponse[i]));
    }
    return new ResponseWrapper(res.headers, result, res.status);
  }

  /**
   * Convert a returned JSON object to Friellenge.
   */
  private convertItemFromServer(json: any): Friellenge {
    const entity: Friellenge = Object.assign(new Friellenge(), json);
    return entity;
  }

  /**
   * Convert a Orders to a JSON which can be sent to the server.
   */
  private convert(friellenge: Friellenge): Friellenge {
    const copy: Friellenge = Object.assign({}, friellenge);
    return copy;
  }

  deleteFavoriteByFriellengeIdAndProfileId(friellengeId: number, profileId: number): Observable<Response> {
    return this.http.delete(this.resourceUrlForFavorite +"?friellengeId=" + friellengeId + "&profileId=" + profileId);

  }

  addFavoriteByFriellengeIdAndProfileId(friellengeId: number, profileId: number): Observable<Response> {
    return this.http.post((this.resourceUrlForFavorite +"?friellengeId=" + friellengeId + "&profileId=" + profileId), null);
  }

  getParticipateProfile(profileId: number) {
    return this.http.get(`${this.resourceUrlForParticipate}/${profileId}`).map((res: Response) => {
      const jsonResponse = res.json();

      // console.log(jsonResponse);
      // console.log();
      //
      // // console.log(JSON.parse(res.json()));
      // console.log();
      //
      // this.profileId2 = jsonResponse[0];
      // console.log(this.profileId2);

      return jsonResponse;
    });
  }

  savePicture(myphoto: any): Observable<Response> {
    return this.http.post( this.resourceUrlForPicture, myphoto)
  }

  loadPicture(path: string): Observable<Response> {
    return this.http.get( this.resourceUrlForPicture + "?reference=" + path);
  }
}
