import {BaseEntity} from "../../models";
import {Profile} from "../profile/profile.model";


export class Friellenge implements BaseEntity {
  constructor(
    public id?: number,
    public donat?: string,
    public nameFriellenge?: string,
    public type?: string,
    public descriptionOfFriellenge?: string,
    public friellengeLikes?: number,
    public reference?: string,
    public startDate?: Date,
    public endDate?: Date,
    public status?: string,
    public countOfProfileByFriellenge?: number,
    public countOfCommentByFriellenge?: number,
    public profile?: Profile,
    public isLiked?: boolean,
    public isFavorite?: boolean,
    public remains?: any,
    public isJoinIn?: boolean,
    public picture?: any,
  ) {
  }
}
