import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FriellengePage } from './friellenge';
import {FriellengeService} from "./friellenge.service";
// import {SimpleTimerPageModule} from "../simple-timer/simple-timer.module";
// import {SimpleTimerPage} from "../simple-timer/simple-timer";
import {PipesModule} from "../../pipes/pipes.module";
import {ReportPageModule} from "../report/report.module";

@NgModule({
  declarations: [
    FriellengePage,
  ],
  imports: [
    IonicPageModule.forChild(FriellengePage),
    PipesModule,
   ],
  exports: [
    FriellengePage
  ],
  providers: [
    FriellengeService,

  ]
})
export class FriellengePageModule { }
