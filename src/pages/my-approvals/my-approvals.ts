import { Component } from '@angular/core';
import {Events, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Profile} from "../profile/profile.model";
import {ApprovalModel} from "../approval/approval.model";
import {ApprovalServiceProvider} from "../../providers/approval-service/approval-service";
import {ResponseWrapper} from "../../models/response-wrapper.model";
import {Friellenge} from "../friellenge/friellenge.model";
import {FriellengeService} from "../friellenge/friellenge.service";

/**
 * Generated class for the MyApprovalsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-approvals',
  templateUrl: 'my-approvals.html',
})
export class MyApprovalsPage {

  approvals: ApprovalModel[];

  profile: Profile = JSON.parse(localStorage.getItem("profile"));


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              private friellengeService: FriellengeService,
              private approvalService: ApprovalServiceProvider,
              public events: Events
) {
    events.subscribe('reloadAllApprovals', () => {
      console.log('Reload All Approvals');
      this.loadAll();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyApprovalsPage');
  }

  ngOnInit() {
    this.loadAll();
  }

  openFriellenge(friellenge: Friellenge) {

    // this.friellengeService.find(friellenge.id).subscribe(
    //   (res: Friellenge) => {
    //     console.log("my approvals ", res);
    //
    //     this.navCtrl.push('OpenFriellengePage', {friellenge: res})
    //   },);

    this.friellengeService.findOne(friellenge.id).subscribe(
      (res: Friellenge) => {
        console.log("my approvals ", res);

        this.navCtrl.push('OpenFriellengePage', {friellenge: res})
      },);

  }

  loadAll() {
    this.approvalService.getByProfile(this.profile.id).subscribe(
      (res: ResponseWrapper) => {
        this.approvals = res.json;
        this.loadPicture();
        this.loadPictureFriellenge();
        console.log("All Approvals ",this.approvals);
      },);
  }


  private getPictureFriellenge(friellenge: Friellenge) {
    return this.friellengeService.loadPicture(friellenge.reference).subscribe((res) => {
      friellenge.reference = 'data:image/jpeg;base64,' + res.text();
    });
  }

  private getPicture(approval: ApprovalModel) {
    return this.friellengeService.loadPicture(approval.reference).subscribe( (res) => {
      approval.reference = 'data:image/jpeg;base64,' + res.text();
    });
  }

  private loadPicture() {
    for (let entry of this.approvals) {
      this.getPicture(entry);
    }
  }

  private loadPictureFriellenge() {
    for (let entry of this.approvals) {
      this.getPictureFriellenge(entry.friellenge);
    }
  }

}
