import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyApprovalsPage } from './my-approvals';
import {FriellengePageModule} from "../friellenge/friellenge.module";

@NgModule({
  declarations: [
    MyApprovalsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyApprovalsPage),
    FriellengePageModule
  ],
})
export class MyApprovalsPageModule {}
