import { Component, ViewChild } from '@angular/core';
import {Events, IonicPage, Nav, NavController, NavParams} from 'ionic-angular';
import {Profile} from "../profile/profile.model";
import {FriellengeService} from "../friellenge/friellenge.service";


export interface PageInterface {
  title: string;
  pageName: string;
  tabComponent?: any;
  index?: number;
}

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html'
})
export class MenuPage {
  // A reference to the ion-nav in our component
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'TabsPage';
  activePage: any;
  // profile: Profile;
  profile =JSON.parse(localStorage.getItem("profile"));

  pagesProfile: PageInterface = { title: 'Profile', pageName: 'TabsPage', tabComponent: 'ProfilePage', index: 4 };

  pages: PageInterface[] = [
    { title: 'Welcome', pageName: 'WelcomePage' },
    { title: 'Login', pageName: 'LoginPage' },
    { title: 'Все', pageName: 'TabsPage', tabComponent: 'FriellengePage', index: 0 },
    { title: 'Активные', pageName: 'TabsPage', tabComponent: 'FriellengePage', index: 1 },
    { title: 'Избранные', pageName: 'TabsPage', tabComponent: 'FriellengePage', index: 2 },
    { title: 'Мои', pageName: 'TabsPage', tabComponent: 'FriellengePage', index: 3 },
    { title: 'Подтверждения', pageName: 'TabsPage', tabComponent: 'MyApprovalsPage', index: 5 },
    { title: 'Лучшие', pageName: 'FavouritePage'},
    { title: 'Settings', pageName: 'SettingsPage' },
  ];


  constructor(public events: Events, public navCtrl: NavController, public navParams: NavParams, public friellengeService: FriellengeService) {
    // events.subscribe('profile', (profile1) => {
    //   this.profile = profile1;
    //   console.log('Profile is here', this.profile);
    // });
    // used for an example of ngFor and navigation
    this.activePage = this.pages[0];
  }

  ngOnInit() {
    this.getPicture(this.profile);
  }

   openPage(page: PageInterface) {
    let params = {};

    // The index is equal to the order of our tabs inside tabs.ts
    if (page.index) {
      params = { tabIndex: page.index };
    }
    // The active child nav is our Tabs Navigation
    if (this.nav.getActiveChildNavs()[0] && page.index != undefined) {
      this.nav.getActiveChildNavs()[0].select(page.index);
    } else {
      // Tabs are not active, so reset the root page
      // In this case: moving to or from SpecialPage
      console.log("page changed");
      this.nav.setRoot(page.pageName, params);
      this.activePage = page;
    }
  }
  checkActive(page){
    return page == this.activePage;
  }

  isActive(page: PageInterface) {
    // Again the Tabs Navigation
    let childNav = this.nav.getActiveChildNavs()[0];

    if (childNav) {
      if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
        return 'secondary';
      }
      return;
    }

    // Fallback needed when there is no active childnav (tabs not active)
    if (this.nav.getActive() && this.nav.getActive().name === page.pageName) {
      return 'secondary';
    }
    return;
  }

  private getPicture(profile: Profile) {
    return this.friellengeService.loadPicture(this.profile.linkPhoto).subscribe( (res) => {
      profile.linkPhoto = 'data:image/jpeg;base64,' + res.text();
    });
  }

}
