import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OpenFriellengePage } from './open-friellenge';
// import {FriellengePage} from "../friellenge/friellenge";
import {FriellengePageModule} from "../friellenge/friellenge.module";

@NgModule({
  declarations: [
    OpenFriellengePage,
  ],
  imports: [
    IonicPageModule.forChild(OpenFriellengePage),
    FriellengePageModule
  ],
  exports: [
    OpenFriellengePage
  ]
})
export class OpenFriellengePageModule {}
