import { Component } from '@angular/core';
import {
  AlertController, Events, IonicPage, NavController, NavParams, PopoverController,
  ToastController
} from 'ionic-angular';
import {Friellenge} from "../friellenge/friellenge.model";
import {FriellengeService} from "../friellenge/friellenge.service";
// import {ResponseWrapper} from "../../models/response-wrapper.model";
import {Profile} from "../profile/profile.model";
import {ApprovalModel} from "../approval/approval.model";
import {ApprovalServiceProvider} from "../../providers/approval-service/approval-service";
import {ResponseWrapper} from "../../models/response-wrapper.model";
import {CommentProvider} from "../../providers/comment/comment";
import {CommentModel} from "../comment/comment.model";
import {PopMenuOfFriAuthorComponent} from "../../components/pop-menu-of-fri-author/pop-menu-of-fri-author";
import {PopMenuOfFriComponent} from "../../components/pop-menu-of-fri/pop-menu-of-fri";
import {ReportService} from "../report/report.service";
import {PopMenuOfComAuthorComponent} from "../../components/pop-menu-of-com-author/pop-menu-of-com-author";
import {PopMenuOfComComponent} from "../../components/pop-menu-of-com/pop-menu-of-com";
import {CommentPage} from "../comment/comment";

/**
 * Generated class for the OpenFriellengePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  providers: [CommentPage],
  selector: 'page-open-friellenge',
  templateUrl: 'open-friellenge.html',
})
export class OpenFriellengePage {
  friellenge: Friellenge;
  profile: Profile;
  approvals: ApprovalModel[];
  timeInSeconds: number;
  remainingSeconds: number;
  runTimer: boolean;
  hasStarted: boolean;
  hasFinished: boolean;
  displayTime: string;
  participate: any[];
  approval: ApprovalModel;
  checkBan: boolean;
  periodForEnd = {days: 0, wholeDays: 0, hours: 0, wholeHours: 0, minutes: 0, wholeMinutes: 0, seconds: 0};
  periodForStatr = {days: 0, wholeDays: 0, hours: 0, wholeHours: 0, minutes: 0, wholeMinutes: 0, seconds: 0};

  comment: CommentModel[];
  newComment: CommentModel = new CommentModel(null, '',);
  profileId = JSON.parse(localStorage.getItem("profile"));

  constructor(public approvalService: ApprovalServiceProvider,
              private friellengeService: FriellengeService,
              private commentProvider: CommentProvider,
              public popoverCtrl: PopoverController,
              private alertCtrl: AlertController,
              public navCtrl: NavController,
              public navParams: NavParams,
              public events: Events,
              public toast: ToastController,
              public reportService: ReportService,
              public commentPage: CommentPage) {
    this.friellenge = navParams.get('friellenge');
    console.log('constructor', this.friellenge);
    this.getDate();

    events.subscribe('reloadAllApprovals', () => {
      console.log('Reload All Approvals');
      this.loadAllApprovals();
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OpenFriellengePage');
  }

  ngOnInit() {
    this.profile = JSON.parse(localStorage.getItem("profile"));
    this.loadComments();
    this.loadAllApprovals();
    this.participateProfile();
    this.getPictureProfile(this.friellenge.profile)

  }

  openProfile(profile: Profile) {
    this.navCtrl.push('OpenProfilePage', {profile: profile})
  }

  approvalFriellenge(friellenge: Friellenge) {
    this.navCtrl.push('ApprovalPage', {friellenge: friellenge})
  }

  addLike() {

    if (this.friellenge.isLiked) {
      this.friellenge.friellengeLikes--;
      this.friellenge.isLiked = false;
      this.friellengeService.deleteLikeByFriellengeIdAndProfileId(this.friellenge.id, this.profile.id).subscribe();
      this.friellengeService.update(this.friellenge).subscribe(value => {
        if (value.statusText !== "OK") {
          this.friellenge.friellengeLikes++;
          this.friellenge.isLiked = true;
        }
      });
    }
    else if (!this.friellenge.isLiked) {
      this.friellenge.friellengeLikes++;
      this.friellenge.isLiked = true;
      this.friellengeService.addLikeByFriellengeIdAndProfileId(this.friellenge.id, this.profile.id).subscribe();
      this.friellengeService.update(this.friellenge).subscribe(value => {
        if (value.statusText !== "OK") {
          this.friellenge.friellengeLikes--;
          this.friellenge.isLiked = false;
        }
      });
    }

  }

  addApprovalLike(approval: ApprovalModel) {

    if (!approval.liked) {
      this.approvalService.addLikeByApprovalIdAndProfileId(approval.id, this.profile.id).subscribe(value => {
          console.log('add ', value.statusText);
          if (value.statusText == "OK") {
            approval.approvalLikes++;
          }
        }
      );
    } else {
      this.approvalService.deleteLikeByApprovalIdAndProfileId(approval.id, this.profile.id).subscribe(value => {
          console.log('del ', value.statusText);
          if (value.statusText == "OK") {
            approval.approvalLikes--;
          }
        }
      );
    }
    approval.liked = !approval.liked;
  }

  addToFavorite() {

    if (this.friellenge.isFavorite) {
      this.friellenge.isFavorite = false;
      this.friellengeService.deleteFavoriteByFriellengeIdAndProfileId(this.friellenge.id, this.profile.id).subscribe();
      this.friellengeService.update(this.friellenge).subscribe(value => {
        if (value.statusText !== "OK") {
          this.friellenge.isFavorite = true;
        }
      });
    }
    else if (!this.friellenge.isFavorite) {
      this.friellenge.isFavorite = true;
      this.friellengeService.addFavoriteByFriellengeIdAndProfileId(this.friellenge.id, this.profile.id).subscribe();
      this.friellengeService.update(this.friellenge).subscribe(value => {
        if (value.statusText !== "OK") {
          this.friellenge.isLiked = false;
        }
      });
    }

  }

  leaveIt() {
    this.approvalService.deleteApprovalsByFriellengeIdAndProfileId(this.friellenge.id, this.profile.id)
      .subscribe(
        () => {
          console.log("Фриленж покинут");
          this.friellenge.isJoinIn = false;
          this.friellenge.countOfProfileByFriellenge--;
        },
        (res: ResponseWrapper) => {
          console.log("Status ERROR", res);
          this.showToast("Ошибка сервера");
        },
        () => {
          this.events.publish('reloadAllApprovals');
          this.events.publish('reloadAll');
        }
      );
  }

  joinIn(friellenge: Friellenge) {

    console.log(friellenge);
    this.approval = new ApprovalModel(null);
    this.approval.profile = new Profile();
    this.approval.friellenge = new Friellenge();
    this.approval.profile.id = this.profile.id;
    this.approval.friellenge.id = friellenge.id;
    this.approval.comment = '';
    this.approval.reference = '';
    this.approval.confirmation = false;
    this.approvalService.addNewApproval(this.approval).subscribe(
      () => {
        console.log("Присоединен Фриленж");
        friellenge.isJoinIn = true;
        friellenge.countOfProfileByFriellenge++;
      },
      (res: ResponseWrapper) => {
        console.log("Status ERROR", res);
        this.showToast("Ошибка сервера");
      },
      () => {
        this.events.publish('reloadAllApprovals');
        this.events.publish('reloadAll');
      }
    );
  }

  getDate() {
    console.log("getdate", this.friellenge);

    let endDate = this.friellenge.endDate;
    let startDate = this.friellenge.startDate;

    this.calculusDate(endDate, this.periodForEnd);
    this.calculusDate(startDate, this.periodForStatr);


    if (this.friellenge.status === 'Finished') {
      this.friellenge.remains = 'Finished'
    }
    else if (this.friellenge.status === 'Deffered') {
      this.friellenge.remains = ("Days for start " + this.periodForStatr.wholeDays.toLocaleString());
    }

    else if (this.periodForEnd.wholeDays >= 1 && this.friellenge.status === 'InProcess') {
      this.friellenge.remains = ("Days of friellenge " + (this.periodForEnd.wholeDays + 1).toLocaleString());
    }

    else if (this.periodForEnd.wholeDays == 0 && this.periodForEnd.wholeHours >= 1 && this.friellenge.status === 'InProcess') {
      console.log('More than month');
      this.friellenge.remains = ("Hours of friellenge " + this.periodForEnd.wholeHours.toLocaleString());
    }

    else if (this.periodForEnd.wholeDays == 0 && this.periodForEnd.wholeHours == 0, this.periodForEnd.wholeMinutes > 5 && this.friellenge.status === 'InProcess') {
      console.log('More than month');
      this.friellenge.remains = ("Minutes of friellenge " + this.periodForEnd.wholeMinutes.toLocaleString());
    }

    else if (this.periodForEnd.minutes <= 5 && this.friellenge.status === 'InProcess') {
      console.log("second " + this.periodForEnd.seconds);
      console.log("min " + this.periodForEnd.minutes);
      console.log("sec + mim " + ((this.periodForEnd.wholeMinutes * 60) + this.periodForEnd.seconds));

      this.setTime((this.periodForEnd.wholeMinutes * 60) + this.periodForEnd.seconds);
      this.timers(this.friellenge);

      this.friellenge.remains = "Time to end";
      // frie.remains = this.getSecondsAsDigitalClock(this.periodForEnd.seconds);
    }
    else {
      console.log('Some error');
      this.friellenge.remains = 'Some error';
    }
    console.log(this.friellenge.remains);

  }

  getSecondsAsDigitalClock(inputSeconds: number) {
    var sec_num = parseInt(inputSeconds.toString(), 10);
    var hours = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);
    var hoursString = '';
    var minutesString = '';
    var secondsString = '';
    // hoursString = (hours < 10) ? "0" + hours : hours.toString();
    minutesString = (minutes < 10) ? "0" + minutes : minutes.toString();
    secondsString = (seconds < 10) ? "0" + seconds : seconds.toString();
    return minutesString + ':' + secondsString;
  }

  calculusDate(date: Date, period: any) {
    let time = new Date(date).getTime() - new Date().getTime();
    period.days = time / 1000 / 60 / 60 / 24;
    period.wholeDays = Math.floor(period.days);
    period.hours = (period.days - period.wholeDays) * 24;
    period.wholeHours = Math.floor(period.hours);
    period.minutes = (period.hours - period.wholeHours) * 60;
    period.wholeMinutes = Math.floor(period.minutes);
    period.seconds = Math.floor((period.minutes - period.wholeMinutes) * 60);


  }

  timers(frie: Friellenge) {

    setTimeout(() => {
      this.startTimer(frie);
    }, 1000);

    // this.timeInSeconds=300;
    this.initTimer();
  }

  initTimer() {
    if (!this.timeInSeconds) {
      this.timeInSeconds = 0;
    }

    this.runTimer = false;
    this.hasStarted = false;
    this.hasFinished = false;
    this.remainingSeconds = this.timeInSeconds;
    this.displayTime = this.getSecondsAsDigitalClock(this.remainingSeconds);
  }

  startTimer(frie: Friellenge) {
    this.runTimer = true;
    this.hasStarted = true;
    this.timerTick(frie);
  }

  timerTick(frie: Friellenge) {
    setTimeout(() => {
      if (!this.runTimer) {
        return;
      }
      this.remainingSeconds--;
      this.displayTime = this.getSecondsAsDigitalClock(this.remainingSeconds);
      if (this.remainingSeconds > 0) {
        this.timerTick(frie);
      }
      else {
        this.hasFinished = true; //todo update friellenge
        frie.status = 'Finished';
      }
    }, 1000);
  }

  setTime(time) {
    this.timeInSeconds = time;
  }

  loadComments() {
    this.commentProvider.query(this.friellenge.id).subscribe(
      (res: ResponseWrapper) => {
        this.comment = res.json;
        this.loadPictureProfileComment();
      },);
  }

  save() {
    if (this.newComment.comment !== '') {
      this.newComment.commentDate = new Date;
      this.newComment.friellenge = new Friellenge(this.friellenge.id);
      this.profile = JSON.parse(localStorage.getItem("profile"));
      this.newComment.profile = this.profile;
      console.log(this.newComment);
      this.commentProvider.addNewComment(this.newComment).subscribe(
        (res: ResponseWrapper) => {
          console.log("fine ", res);
        },
        (res: ResponseWrapper) => {
          console.log("err ", res);
        },
        () => {
          console.log("compl ");
          this.loadComments();
          this.newComment.comment = "";
        }
      );
    }
  }

  getCommentDate(date: Date) {
    let da = new Date(date);
    return da.toLocaleString().substr(0, 17);
  }

  participateProfile() {
    this.friellengeService.getParticipateProfile(this.profile.id).subscribe((res: any) => {
      this.participate = res;
      // loop1:
      for (let id of this.participate) {
        console.log("participate id", id);

        if (id == this.friellenge.id) {
          this.friellenge.isJoinIn = true;
          console.log(this.friellenge.isJoinIn);
          break;
        }
      }
    })
  }

  presentPopover(event, friellenge: Friellenge) {
    let popover;
    if (friellenge.profile.id === this.profile.id) {
      popover = this.popoverCtrl.create(PopMenuOfFriAuthorComponent);
      popover.present({
        ev: event
      });
    }
    else {
      popover = this.popoverCtrl.create(PopMenuOfFriComponent);
      popover.present({
        ev: event
      });
    }
    popover.onDidDismiss(popoverData => {
      if (popoverData != null && popoverData.item == 'Репорт')
        this.doReport(friellenge, null, null);
      else if (popoverData != null && popoverData.item == 'Удалить')
        this.deleteFriellenge(friellenge.id);
    })
  }

  presentPopoverApproval(event, approval: ApprovalModel) {
    let popover;
    if (approval.profile.id === this.profile.id) {
      popover = this.popoverCtrl.create(PopMenuOfFriAuthorComponent);
      popover.present({
        ev: event
      });
    }
    else {
      popover = this.popoverCtrl.create(PopMenuOfFriComponent);
      popover.present({
        ev: event
      });
    }
    popover.onDidDismiss(popoverData => {
      if (popoverData != null && popoverData.item == 'Репорт')
        this.doReportApproval(this.friellenge, approval, null);
      else if (popoverData != null && popoverData.item == 'Удалить')
        this.deleteApproval(approval);
    })
  }

  presentPopoverComment(event, comment: CommentModel) {
    let popover;
    if (comment.profile.id === this.profileId.id) {
      popover = this.popoverCtrl.create(PopMenuOfComAuthorComponent);
      console.log(popover);
      popover.present({
        ev: event
      });
    }
    else {
      popover = this.popoverCtrl.create(PopMenuOfComComponent);
      console.log("Com",popover);
      popover.present({
        ev: event
      });
    }
    popover.onDidDismiss(popoverData => {
      if (popoverData != null && popoverData.item == 'Репорт')
        this.doReportComment(this.friellenge, null, comment);
       else if (popoverData != null && popoverData.item == 'Удалить')
        this.commentPage.deleteComment(comment.id);
    });
  }


  private doReport(item: Friellenge, approval: ApprovalModel, comment: CommentModel) { // todo it is need finish this method
    this.reportService.showCheckbox(item, approval, comment);
  }

  private doReportApproval(item: Friellenge, approval: ApprovalModel, comment: CommentModel) {
    this.reportService.showCheckbox(item, approval, comment);
  }

  private doReportComment(item: Friellenge, approval: ApprovalModel, comment: CommentModel) {
    this.reportService.showCheckbox(item, approval, comment);
  }


  private deleteFriellenge(friellengeId: number) {
    this.friellengeService.deleteFriellengeById(friellengeId).subscribe(
      (value) => {
        if (value.statusText == "OK") {
          this.showToast("Фриленж удален");
        }
      },
      (res: ResponseWrapper) => {
        console.log("Status ERROR", res);
        this.showToast("Ошибка сервера");
      },
      () => {
        console.log("Delete and Load All");
        this.events.publish('reloadAll');
        this.navCtrl.popToRoot();
      }
    );

  }

  private deleteApproval(approval: ApprovalModel) {
    approval.comment = "";
    approval.reference = "";
    approval.confirmation = false;
    this.approvalService.update(approval).subscribe(
      (value) => {
        if (value.statusText == "OK") {
          this.approvalService.deleteAllLikesByApproval(approval.id).subscribe(
            (val) => {
              if (val.statusText == "OK") {
                console.log('likes deleted');
                this.showToast("Аппрувал удален");
              }
            },
            (res: ResponseWrapper) => {
              console.log("Status ERROR", res);
              this.showToast("Ошибка сервера");
            }
          );
        }
      },
      (res: ResponseWrapper) => {
        console.log("Status ERROR", res);
        this.showToast("Ошибка сервера");
      },
      () => {
        console.log("Delete and Load All");
        this.loadAllApprovals();
      }
    );

  }

  showToast(name: string) {
    const toast = this.toast.create({
      message: `${name}`,
      duration: 2000
    });
    toast.present();
  }

  loadAllApprovals() {
    this.approvalService.getByFriellenge(this.friellenge.id, this.profile.id).subscribe(
      (res: ResponseWrapper) => {
        this.approvals = res.json;
        this.loadPictureApprovals();
        this.loadPictureProfileApproval();
        console.log("All Approvals ", this.approvals);
      },);
  }

  private getPictureProfile(profile: Profile) {
    return this.friellengeService.loadPicture(profile.linkPhoto).subscribe((res) => {
      profile.linkPhoto = 'data:image/jpeg;base64,' + res.text();
    });
  }

  private getPicture(approval: ApprovalModel) {
    return this.friellengeService.loadPicture(approval.reference).subscribe((res) => {
      approval.reference = 'data:image/jpeg;base64,' + res.text();
    });
  }

  private loadPictureApprovals() {
    for (let entry of this.approvals) {
      this.getPicture(entry);
    }
  }

  private loadPictureProfileApproval() {
    for (let entry of this.approvals) {
      this.getPictureProfile(entry.profile);
    }
  }

  private loadPictureProfileComment() {
    for (let entry of this.comment) {
      this.getPictureProfile(entry.profile);
    }
  }
}
