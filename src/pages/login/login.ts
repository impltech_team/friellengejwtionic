import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';
import { LoginService } from '../../providers/login/login.service';
import { ProfilePage } from "../profile/profile";
import { ProfileService } from "../profile/profile.service";

@IonicPage()
@Component({
providers: [ProfileService,ProfilePage],
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  // The account fields for the login form.
  account: { username: string, password: string, rememberMe: boolean } = {
    username: '',
    password: '',
    rememberMe: false,
  };

  // Our translated text strings
  private loginErrorString: string;

  constructor(public navCtrl: NavController,
    public loginService: LoginService,
    public toastCtrl: ToastController,
    public translateService: TranslateService,
    public profPage: ProfilePage,
  ) {

    this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })
  }

  // Attempt to login in through our User service
  doLogin() {
    this.loginService.login(this.account).then((response) => {
      console.log(response.toString());
      localStorage.setItem("FB_ID", "1114912548651106");//hardcode , del string, if working with fb login
      this.profPage.loadProfile(Number(localStorage.getItem("FB_ID")));
      this.navCtrl.setRoot('MenuPage');
      // this.navCtrl.push(Friellenge);
    }, (err) => {
      // Unable to log in
      this.account.password = '';
      let toast = this.toastCtrl.create({
        message: this.loginErrorString,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    });
  }
}
// 1787932851271481 Andry
// 1114912548651106 Pasha
// 1716857261686684 Slava
// 551824511854222 Lubomyr
