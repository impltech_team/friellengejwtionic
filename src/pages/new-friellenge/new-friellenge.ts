import {Component} from '@angular/core';
import {AlertController, Events, IonicPage, Nav, NavController, NavParams, ToastController} from 'ionic-angular';
import {FriellengeService} from "../friellenge/friellenge.service";
import { PopoverController } from 'ionic-angular';
import { Camera, CameraOptions } from '@ionic-native/camera';
import {PoptemplComponent} from "../../components/poptempl/poptempl";
import {PopnewfriComponent} from "../../components/popnewfri/popnewfri";
import {Template} from "../template/template.model";
import {Friellenge} from "../friellenge/friellenge.model";
import {Profile} from "../profile/profile.model";
import {ResponseWrapper} from "../../models/response-wrapper.model";
import {ProfileService} from "../profile/profile.service";
import {ApprovalModel} from "../approval/approval.model";
import {ApprovalServiceProvider} from "../../providers/approval-service/approval-service";
import {Response} from "@angular/http";

/**
 * Generated class for the NewFriellengePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  providers: [FriellengeService],
  selector: 'page-new-friellenge',
  templateUrl: 'new-friellenge.html',
})
export class NewFriellengePage {

  friends: Profile[];
  invitedFriends: Profile[]=[];
  newFriellenge: Friellenge = new Friellenge(null,);
  templateItem: Template;
  profile = JSON.parse(localStorage.getItem("profile"));
  myphoto:any = "assets/img/screen2.png";
  nameTemplate: string;
  descriptionTemplate: string;
  typeOfFriellenge:string="Open";
  donat:number=0.00;
  isDonat:boolean=false;
  isTypeOpen:boolean=true;
  isTypeFriends:boolean=false;
  isTypePrivate:boolean=false;
  startDate;
  finishDate;
  minDateStart;
  maxDateStart;
  minDateFinish;
  maxDateFinish;
  imageData: any;


  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public popoverCtrl: PopoverController,
              private camera: Camera,
              public friellengeService: FriellengeService,
              private profileService: ProfileService,
              public approvalService: ApprovalServiceProvider,
              private alertCtrl: AlertController,
              public nav: Nav,
              public events: Events,
              public toast: ToastController
  ) {
    this.templateItem = navParams.get('templateItem');

    if(this.templateItem != null){
      this.myphoto=this.templateItem.contentReference;
      this.nameTemplate=this.templateItem.nameOfTemplate;
      this.descriptionTemplate=this.templateItem.description;
    }
  }

  ngOnInit() {
    this.getPictureProfile(this.profile);
    this.initData();
    this.loadAllFrinds();
  }

  initData(){
    this.startDate  = new Date().toISOString();
    this.minDateStart = new Date().toISOString();
    this.maxDateStart = new Date(new Date(this.startDate).getTime()+2678400000).toISOString();
    this.minDateFinish = new Date(new Date(this.startDate).getTime()+3600000).toISOString();
    this.maxDateFinish = new Date(new Date(this.minDateFinish).getTime()+2678400000).toISOString();
    this.finishDate  = new Date(new Date(this.startDate).getTime()+3600000).toISOString();
  }

  initStartDate(){
    this.minDateFinish = new Date(new Date(this.startDate).getTime()+3600000).toISOString();
    this.maxDateFinish = new Date(new Date(this.minDateFinish).getTime()+2678400000).toISOString();
    this.finishDate  = new Date(new Date(this.startDate).getTime()+3600000).toISOString();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad NewFriellengePage');
  }

  presentPopover(myEvent, source) {
    let popover = this.popoverCtrl.create(PopnewfriComponent);
    popover.present({
      ev: myEvent
    });

    popover.onDidDismiss(popoverData =>{
      if(popoverData !== null) {
        if (popoverData.item == 'Галерея')
          this.getImage();
        else if (popoverData.item == 'Камера')
          this.takePhoto();
      }
    })
  }

  presentPopoverTempl(myEvent) {
    let popover = this.popoverCtrl.create(PoptemplComponent);
    popover.present({
      ev: myEvent
    });

    popover.onDidDismiss(popoverData =>{
      if(popoverData !== null) {
        if (popoverData.item == 'друзья')
          this.navCtrl.push('TemplatePage', {typeOfTemplate: "Friends"});
        else if (popoverData.item == 'работа')
          this.navCtrl.push('TemplatePage', {typeOfTemplate: "Work"});
        else if (popoverData.item == 'учеба')
          this.navCtrl.push('TemplatePage', {typeOfTemplate: "Study"});
        else if (popoverData.item == 'любовь')
          this.navCtrl.push('TemplatePage', {typeOfTemplate: "Love"});
        else if (popoverData.item == 'семья')
          this.navCtrl.push('TemplatePage', {typeOfTemplate: "Family"});
        else if (popoverData.item == 'прочее')
          this.navCtrl.push('TemplatePage', {typeOfTemplate: "Other"});
        else if (popoverData.item == 'свой')
          this.navCtrl.push('TemplatePage', {typeOfTemplate: "My"});
      }
    })
  }

  takePhoto(){
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum:true,
      correctOrientation: true,
      allowEdit:true
    };

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.imageData = imageData;
      this.myphoto = 'data:image/jpeg;base64,' + imageData;

    }, (err) => {
      // Handle error
    });
  }

  getImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum:false,
      allowEdit:true
    };
    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.imageData = imageData;
      this.myphoto = 'data:image/jpeg;base64,' + imageData;

    }, (err) => {
      // Handle error
    });
  }

  start(){
    if(this.nameTemplate != null &&
      this.nameTemplate != '' &&
      this.descriptionTemplate != null &&
      this.descriptionTemplate != ''){


      this.newFriellenge.nameFriellenge=this.nameTemplate;
      this.newFriellenge.descriptionOfFriellenge=this.descriptionTemplate;
      this.newFriellenge.profile=new Profile(this.profile.id);
      this.newFriellenge.donat=this.donat.toString();
      this.newFriellenge.type=this.typeOfFriellenge;
      this.newFriellenge.status="InProcess";
      this.newFriellenge.friellengeLikes=0;
      this.newFriellenge.startDate=new Date(new Date(this.startDate).setMilliseconds(0));
      this.newFriellenge.endDate=new Date(new Date(this.finishDate).setMilliseconds(0));

      this.friellengeService.savePicture(this.imageData).subscribe( (res: Response) => {
        if (res.statusText == 'OK') {
          this.newFriellenge.reference = res.text();

      console.log("New Friellenge ",this.newFriellenge);

      this.friellengeService.create(this.newFriellenge).subscribe((value) => {
      console.log(value);
        if(value.statusText=="OK"){
          for(let friend of this.invitedFriends) {
            console.log("JOIN ", value.json());
            let approval = new ApprovalModel(null);
            approval.profile = new Profile();
            approval.friellenge = new Friellenge();
            approval.profile.id = friend.id;
            approval.friellenge.id = value.json();
            approval.comment = '';
            approval.reference = '';
            approval.confirmation = false;
            this.approvalService.addNewApproval(approval).subscribe(
              () => {
                console.log("Add Approval OK ",friend.id);
              },
              (res: ResponseWrapper) => {
                console.log("Add Approval ERROR", res);
                console.log("ERROR in ",friend.id);
                this.showToast("Ошибка сервера");
              },
              () => {
              }
            );
          }
        }
      },
        err => {
          console.log(err);
          this.showToast("Ошибка сервера");
        },
        ()=>{
          this.showToast("Фриленж сохранен");
          this.events.publish('loadAll');
          this.navCtrl.popToRoot();
        });
        }
      });
    }
    else {
      let alert = this.alertCtrl.create({
        title: 'Ошибка создания',
        subTitle: 'Заполните все поля',
        buttons: ['OK']
      });
      alert.present();
    }

  }

  typeOpen(){
    this.typeOfFriellenge="Open";
    this.isTypeOpen = true;
    this.isTypeFriends = false;
    this.isTypePrivate = false;
    console.log("TypeOfFriellenge ",this.typeOfFriellenge);
  }

  typeFriends(){
    this.typeOfFriellenge="Friends";
    this.isTypeOpen = false;
    this.isTypeFriends = true;
    this.isTypePrivate = false;
    console.log("TypeOfFriellenge ",this.typeOfFriellenge);
  }

  typePrivate(){
    this.typeOfFriellenge="Private";
    this.isTypeOpen = false;
    this.isTypeFriends = false;
    this.isTypePrivate = true;
    console.log("TypeOfFriellenge ",this.typeOfFriellenge);
  }

  donatInput(){
    this.isDonat=!this.isDonat;
    if (this.isDonat!=true){
      this.donat=0;
    }
  }

  showToast(name: string){
    const toast = this.toast.create({
      message:`${name}`,
      duration: 2000
    });
    toast.present();
  }

  loadAllFrinds() {
    this.profileService.getFriends(this.profile.id).subscribe((res: ResponseWrapper) => {
      let friendsListIn = res.json;
      let friendsListOut = [];
      console.log("friends IN ",friendsListIn);

      for(let friend of friendsListIn) {
        this.profileService.findID(friend.id).subscribe((profile) => {
          friendsListOut.push(profile);
          this.getPictureProfile(profile)
        })
      }
      console.log("friends OUT",friendsListOut);
      this.friends=friendsListOut;
    })
  }

  addToNewFriellenge(friend: Profile){
    console.log("Friend ",friend);
    let index = this.friends.indexOf(friend,0);
    this.friends[index].isInvite=true;
    this.invitedFriends.push(friend);
    console.log("list friends after add ",this.invitedFriends);
  }

  deleteFromNewFriellenge(friend: Profile){
    let index = this.invitedFriends.indexOf(friend, 0);
    console.log("index ",index);
    if (index > -1) {
      this.invitedFriends.splice(index, 1);
      let indexFriend = this.friends.indexOf(friend,0);
      this.friends[indexFriend].isInvite=false;
    }
    console.log("list friends after del ",this.invitedFriends);

  }

  openProfile(profile: Profile) {
    this.navCtrl.push('OpenProfilePage', {profile: profile})
  }


  private getPictureProfile(profile: Profile) {
    return this.friellengeService.loadPicture(profile.linkPhoto).subscribe( (res) => {
      profile.linkPhoto = 'data:image/jpeg;base64,' + res.text();
    });
  }
}

