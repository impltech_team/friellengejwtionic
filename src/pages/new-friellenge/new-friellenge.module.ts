import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NewFriellengePage } from './new-friellenge';

@NgModule({
  declarations: [
    NewFriellengePage,
  ],
  entryComponents:[
  ],
  imports: [
    IonicPageModule.forChild(NewFriellengePage)
  ]
})
export class NewFriellengePageModule {}

