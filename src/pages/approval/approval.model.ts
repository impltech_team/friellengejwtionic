import {BaseEntity} from "../../models";
import {Profile} from "../profile/profile.model";
import {Friellenge} from "../friellenge/friellenge.model";


export class ApprovalModel implements BaseEntity {
  constructor(
    public id?: number,
    public reference?: string,
    public comment?: string,
    public confirmation?: boolean,
    public profile?: Profile,
    public friellenge?: Friellenge,
    public liked?: boolean,
    public approvalLikes?: number,
  ) {
  }
}
