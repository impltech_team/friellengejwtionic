import {Component} from '@angular/core';
import {
  AlertController, Events, IonicPage, NavController, NavParams, PopoverController,
  ToastController
} from 'ionic-angular';
import {Friellenge} from "../friellenge/friellenge.model";
import {PopnewfriComponent} from "../../components/popnewfri/popnewfri";
import {Camera, CameraOptions} from '@ionic-native/camera';
import {ApprovalModel} from "./approval.model";
import {ApprovalServiceProvider} from "../../providers/approval-service/approval-service";
import {Response} from "@angular/http";
import {FriellengeService} from "../friellenge/friellenge.service";

/**
 * Generated class for the ApprovalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-approval',
  templateUrl: 'approval.html',
})
export class ApprovalPage {

  profile = JSON.parse(localStorage.getItem("profile"));
  newApproval: ApprovalModel = new ApprovalModel(null,);
  friellenge: Friellenge;
  myphoto: any = "assets/img/screen2.png";
  descriptionApproval: string;
  imageData: any;

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public events: Events,
              public popoverCtrl: PopoverController,
              private camera: Camera,
              public approvalServiceProvider: ApprovalServiceProvider,
              private alertCtrl: AlertController,
              public toast: ToastController,
              public friellengeService: FriellengeService) {
    this.friellenge = navParams.get('friellenge');
    console.log('constructor', this.friellenge);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ApprovalPage');
  }

  ngOnInit() {
    this.loadApproval();
  }

  loadApproval() {
    this.approvalServiceProvider.getByFriellengeAndProfile(this.friellenge.id, this.profile.id).subscribe(
      (approval: ApprovalModel) => {
        if (approval.confirmation == true) {
          this.descriptionApproval = approval.comment;
          this.getPicture(approval);
        }
        console.log("Load Approval ", approval);
        console.log(this.myphoto)
      },);
  }

  presentPopover(myEvent) {
    let popover = this.popoverCtrl.create(PopnewfriComponent);
    popover.present({
      ev: myEvent
    });

    popover.onDidDismiss(popoverData => {
      if (popoverData !== null) {
        if (popoverData.item == 'Галерея')
          this.getImage();
        else if (popoverData.item == 'Камера')
          this.takePhoto();
      }
    });
  }

  takePhoto() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true,
      correctOrientation: true,
      allowEdit: true
    };

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.imageData = imageData;
      this.myphoto = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }

  getImage() {
    const options: CameraOptions = {
      quality: 70,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      saveToPhotoAlbum: false,
      allowEdit: true
    }

    this.camera.getPicture(options).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64:
      this.imageData = imageData;
      this.myphoto = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      // Handle error
    });
  }

  saveApproval() {

    if (this.descriptionApproval != null && this.descriptionApproval != '') {
      this.newApproval.reference = this.myphoto;
      this.newApproval.comment = this.descriptionApproval;
      this.newApproval.confirmation = true;
      this.newApproval.friellenge = this.friellenge;
      this.newApproval.profile = this.profile;

      this.friellengeService.savePicture(this.imageData).subscribe((res: Response) => {
        if (res.statusText == 'OK') {
          this.newApproval.reference = res.text();


          console.log(this.newApproval);

          this.approvalServiceProvider.update(this.newApproval).subscribe((value) => {
              console.log(value);
              if (value.statusText == "OK") {
                this.showToast("Подтверждено!");
                this.events.publish('reloadAllApprovals');
                this.navCtrl.pop();
              }
            },
            err => {
              console.log(err);
              this.showToast("Ошибка сервера");
            }
          );

        }
      });
    }
    else {
      let alert = this.alertCtrl.create({
        title: 'Ошибка создания',
        subTitle: 'Заполните все поля',
        buttons: ['OK']
      });
      alert.present();
    }
  }

  showToast(name: string) {
    const toast = this.toast.create({
      message: `${name}`,
      duration: 2000
    });
    toast.present();
  }


  private getPicture(approval: ApprovalModel) {
    return this.friellengeService.loadPicture(approval.reference).subscribe((res) => {
      this.myphoto = 'data:image/jpeg;base64,' + res.text();
    });
  }
}
